package workflows.crypto;

import org.junit.jupiter.api.Test;

import java.security.*;
import java.security.spec.KeySpec;

public class Signatures extends CryptoBase {

    public Signatures() throws NoSuchAlgorithmException, NoSuchProviderException {
    }

    @Test
    public void public_AndPrivate_Keys() {

    }

    /**
     * Return a public key for algorithm built from the details in keySpec.
     *
     * @param algorithm the algorithm the key specification is for.
     * @param keySpec a key specification holding details of the public key.
     * @return a PublicKey for algorithm
     */
    public static PublicKey createPublicKey(String algorithm, KeySpec keySpec)
            throws GeneralSecurityException {
        KeyFactory keyFact = KeyFactory.getInstance(algorithm, "BC");
        return keyFact.generatePublic(keySpec);
    }

    /**
     * Return a private key for algorithm built from the details in keySpec.
     *
     * @param algorithm the algorithm the key specification is for.
     * @param keySpec a key specification holding details of the private key.
     * @return a PrivateKey for algorithm
     */
    public static PrivateKey createPrivateKey(String algorithm, KeySpec keySpec)
            throws GeneralSecurityException {
        KeyFactory keyFact = KeyFactory.getInstance(algorithm, "BC");
        return keyFact.generatePrivate(keySpec);
    }
}
