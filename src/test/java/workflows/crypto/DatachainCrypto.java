package workflows.crypto;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.converter.bytestream.ByteStreamConverter;
import com.nexus.qrs.converter.bytestream.ByteStreamConverterFactory;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.QRData;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DatachainCrypto extends CryptoBase {

    private IQRSContext IQRSContext = new IQRSContext() {};

    public DatachainCrypto() throws NoSuchAlgorithmException, NoSuchProviderException {
    }

    @Test
    public void simpleFileCrypto() {
        Path fp = getInputFile("helloWorld");

        String inputRaw = getHexBytes(fp);
        assertNotNull(inputRaw);

        // Generate a key
        byte[] keyHex = Hex.decode(IQRSContext.asciiToHex("MySecretKey12345"));
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");
        System.out.printf("secretKey(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(secretKey.getEncoded())));
        System.out.printf("secretKey(hex): %s%n", Hex.toHexString(secretKey.getEncoded()));

        // Create and AES cipher in Cipher Block Chaining (CBC) mode
        //TODO: Consider implementing a Factory for this
        Cipher aesCipher;
        try {
            aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }

        // Generating a random IV, method 1
        byte[] iv = new byte[aesCipher.getBlockSize()];
        random.nextBytes(iv);

        byte[] inputBytes = Hex.decode(inputRaw);
        System.out.printf("input(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(inputBytes)));
        System.out.printf("input(hex): %s%n", Hex.toHexString(inputBytes));

        byte[] encrypted, decrypted;
        try {
            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
            encrypted = aesCipher.doFinal(inputBytes);
            System.out.printf("encrypted(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(encrypted)));
            System.out.printf("encrypted(hex): %s%n", Hex.toHexString(encrypted));

            aesCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            decrypted = aesCipher.doFinal(encrypted);
            System.out.printf("decrypted(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(aesCipher.doFinal(encrypted))));
            System.out.printf("decrypted(hex): %s%n", Hex.toHexString(decrypted));

            assertEquals(Hex.toHexString(inputBytes), Hex.toHexString(decrypted));
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException |
                 InvalidAlgorithmParameterException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void simpleDatachainCrypto() {
        // inputFile.get(0) guarantees a single QR code
        Path fp = getInputFile("helloWorld");
        String inputRaw = getHexBytes(fp);
        int[] size = {400, 400};

        // Generate a key
        byte[] keyHex = Hex.decode(IQRSContext.asciiToHex("MySecretKey12345"));
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");

        Cipher aesCipher;
        try {
            aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }

        // Generating a random IV, method 1
        byte[] iv = new byte[aesCipher.getBlockSize()];
        random.nextBytes(iv);

        byte[] inputBytes = Hex.decode(inputRaw);
        System.out.printf("input(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(inputBytes)));
        System.out.printf("input(hex): %s%n", Hex.toHexString(inputBytes));

        byte[] encrypted, decrypted;
        try {
            // Encrypt the file
            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
            encrypted = aesCipher.doFinal(inputBytes);
            String encryptedHexString = Hex.toHexString(encrypted);

            System.out.printf("encrypted(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(encrypted)));
            System.out.printf("encrypted(hex): %s%n", Hex.toHexString(encrypted));

            // Partition the encrypted data into a DataChain
            FileDataChain dataChain = converter.readEncryptedString(encryptedHexString, fp.toFile().getName(), 2000);
            assertNotNull(dataChain);

            // Generate the QR Codes
            List<QRData> qrCodes = serializer.createQrCodes(dataChain.getContents(),size[0], size[1]);

            // Decode  the first QR code and get the payload
            String payload = deserializer.readQrCode(qrCodes.get(0).getQrImage());
            assertNotNull(payload);

            // At this point, the payload is still partitioned QR data.
            // Rebuild the payload with a method 'reconstructFile()'
            List<List<String>> partitionData = new ArrayList<>();
            List<String> partition = IQRSContext.partition(IQRSContext.stripSequenceCounter(payload));
            partitionData.add(partition);
            byte[] fileData = IQRSContext.reconstructFile(partitionData, "txt");

            aesCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            decrypted = aesCipher.doFinal(fileData);
            System.out.printf("decrypted(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(decrypted)));
            System.out.printf("decrypted(hex): %s%n", Hex.toHexString(decrypted));

            assertEquals(Hex.toHexString(inputBytes), Hex.toHexString(decrypted));
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException |
                 InvalidAlgorithmParameterException | NotFoundException | IOException |
                 FormatException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void fileDatachainCrypto() {
        // inputFile.get(>=1) guarantees multiple QR Codes
        Path fp = getInputFile("hotboxImage");
        String inputRaw = getHexBytes(fp);
        int[] size = {400, 400};

        // Generate a key
        byte[] keyHex = Hex.decode(IQRSContext.asciiToHex("MySecretKey12345"));
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");

        Cipher aesCipher;
        try {
            aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }

        // Generating a random IV, method 1
        byte[] iv = new byte[aesCipher.getBlockSize()];
        random.nextBytes(iv);

        byte[] inputBytes = Hex.decode(inputRaw);
        System.out.printf("input(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(inputBytes)));
        System.out.printf("input(hex): %s%n", Hex.toHexString(inputBytes));

        byte[] encrypted, decrypted;
        try {
            // Encrypt the file
            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
            encrypted = aesCipher.doFinal(inputBytes);
            String encryptedHexString = Hex.toHexString(encrypted);

            System.out.printf("encrypted(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(encrypted)));
            System.out.printf("encrypted(hex): %s%n", Hex.toHexString(encrypted));

            // Partition the encrypted data into a DataChain
            FileDataChain dataChain = converter.readEncryptedString(encryptedHexString, fp.toFile().getName(), 2000);
            assertNotNull(dataChain);

            // Generate the QR Codes
            List<QRData> qrCodes = serializer.createQrCodes(dataChain.getContents(),size[0], size[1]);

            // Retry logic
            int retries = 3;
            List<String> payloads = qrCodes.stream()
                    .map(qrCode -> {
                        try {
                            return deserializer.readQrCode(qrCode.getQrImage());
                        } catch (IOException | NotFoundException | FormatException e) {
                            for (int i = 0; i <= retries; i++) {
                                try {
                                    return deserializer.readQrCode(qrCode.getQrImage());
                                } catch (IOException | NotFoundException | FormatException ex) {
                                    if (i == retries)
                                        throw new RuntimeException(ex);
                                }
                            }
                        }
                        return null;
                    })
                    .collect(Collectors.toList());
            assertNotNull(payloads);

            // At this point, the payload is still partitioned QR data.
            // Rebuild the payload with a method 'reconstructFile()'
            List<List<String>> partitions = payloads.stream()
                            .map(stream -> IQRSContext.partition(IQRSContext.stripSequenceCounter(stream)))
                                    .toList();

            List<List<String>> partitionData = new ArrayList<>(partitions);

            byte[] fileData = IQRSContext.reconstructFile(partitionData, "txt");

            aesCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            decrypted = aesCipher.doFinal(fileData);
            System.out.printf("decrypted(raw): %s%n", IQRSContext.hexToAscii(Hex.toHexString(decrypted)));
            System.out.printf("decrypted(hex): %s%n", Hex.toHexString(decrypted));

            assertEquals(Hex.toHexString(inputBytes), Hex.toHexString(decrypted));
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException |
                 InvalidAlgorithmParameterException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getHexBytes(Path fp) {
        int byteData;
        StringBuilder sb = new StringBuilder();
        ByteStreamConverter converter = ByteStreamConverterFactory.getDefaultConverter();
        try (InputStream is = Files.newInputStream(fp)) {
            while ((byteData = is.read()) != -1) {
                sb.append(converter.convert(byteData));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }
}
