package workflows.crypto;


import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Digests {

    @Test
    public void sha3Test() {
        SHA3.DigestSHA3 md = new SHA3.Digest224();
        md.update("Hello, Digest!".getBytes());
        String hexString = Hex.toHexString(md.digest());
        assertNotNull(hexString);
    }
}
