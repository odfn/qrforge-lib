package workflows.crypto;

import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Ciphers extends CryptoBase {

    public Ciphers() throws NoSuchAlgorithmException, NoSuchProviderException {
    }

    /**
     * The simplest mechanism for converting byte data into a SecretKey for the JCE
     * */
    @Test
    public void secretKeySpec() {

        byte[] bytes = IQRSContext.asciiToHex("This is a test").getBytes();
        byte[] keyBytes = Hex.decode(bytes);
        assertNotNull(keyBytes);

        SecretKey secretKey = new SecretKeySpec(keyBytes, "AES");
        assertNotNull(secretKey);
    }

    /**
     * As an alternative to the SecretKeySpec, it is also possible to generate secret keys for a specific
     * algorithm using the javax.crypto.KeyGenerator class
     * */
    @Test
    public void symmetricKey_generation() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyGenerator kGen = KeyGenerator.getInstance("AES", "BC");
        int[] keyLength = {128, 192, 256};

        // Generate a key length of 128 bits
        kGen.init(keyLength[0], random);
        SecretKey key128 = kGen.generateKey();
        assertNotNull(key128);

        // Generate a key length of 192 bits
        kGen.init(keyLength[1], random);
        SecretKey key192 = kGen.generateKey();
        assertNotNull(key192);

        // Generate a key length of 256 bits
        kGen.init(keyLength[2], random);
        SecretKey key256 = kGen.generateKey();
        assertNotNull(key256);
    }

    /**
     * Electronic Code Book (ECB) mode is the symmetric cipher in its simplest form, and as the name of
     * the mode suggests, the cipher is simply mapping one block of bits to a block of bits in the virtual
     * code book represented by the secret key. The encryption/decryption setting of the cipher determines
     * whether it is cipher text or plain text that is being produced by the operation. Code book mode is
     * really just a lookup in a table, any patterns in the plain text will also appear in the cipher text
     * if they exist on block boundaries. Generally, this is not really the effect we are looking for in
     * encrypting data.
     * */
    @Test
    public void aesCipher_ElectronicCodeBook_Mode()
            throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {

        // Generate the encryption key
        byte[] keyHex = Hex.decode("000102030405060708090a0b0c0d0e0f");
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");

        // Create and AES cipher in Electronic Code Book (ECB) mode
        Cipher aesCipher = Cipher.getInstance("AES/ECB/NoPadding", "BC");

        // Hex encoded input data
        byte[] inputBytes = Hex.decode("a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7" + "a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7");
        System.out.printf("input: %s%n", Hex.toHexString(inputBytes));

        //
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] output = aesCipher.doFinal(inputBytes);
        System.out.printf("encrypted: %s%n", Hex.toHexString(output));

        aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
        System.out.printf("decrypted: %s%n", Hex.toHexString(aesCipher.doFinal(output)));
    }

    /**
     * In order to get away from these patterns, it is necessary to add some randomness to the stream
     * and then allow that to propagate. This is where Cipher Block Chaining (CBC) mode comes in. CBC
     * mode introduces a random initialization vector (IV) into the output stream and, providing the IV
     * is not reused with the same plain text, the cipher text produced will always look different as the
     * randomness of the IV will propagate through the blocks produced by the cipher
     * */
    @Test
    public void aesCipher_CipherBlockChaining_Mode()
            throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

        // Generate the encryption key
        byte[] keyHex = Hex.decode("000102030405060708090a0b0c0d0e0f");
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");
        System.out.printf("secretKey: %s%n", Hex.toHexString(secretKey.getEncoded()));

        // Create and AES cipher in Cipher Block Chaining (CBC) mode
        Cipher aesCipher = Cipher.getInstance("AES/CBC/NoPadding", "BC");

        // Hex encoded input data
        byte[] inputBytes = Hex.decode("a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7"+ "a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7");
        System.out.printf("input: %s%n", Hex.toHexString(inputBytes));

        // Hard coded initialization vector (IV)
        // byte[] iv = Hex.decode("9f741fdb5d8845bdb48a94394e84f8a3"); // - DEMO PURPOSE ONLY! NOT THE BEST PRACTICE!

        // Generating a random IV, method 1
        byte[] iv = new byte[aesCipher.getBlockSize()];
        random.nextBytes(iv);
        System.out.printf("IV: %s%n", Hex.toHexString(iv));

        // Setting the cipher mode to encrypt with the IV
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
        byte[] encrypted = aesCipher.doFinal(inputBytes);
        System.out.printf("encrypted: %s%n", Hex.toHexString(encrypted));

        // Setting the cipher mode to decrypt with the IV
        aesCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
        byte[] decrypted = aesCipher.doFinal(encrypted);
        System.out.printf("decrypted: %s%n", Hex.toHexString(decrypted));
    }

    /**
     * This next example shows how auto-generation of an IV can be done for CBC mode. You will find
     * that any ciphers which require the use of the IV will support something similar.
     * */
    @Test
    public void aesCipher_CipherBlockChaining_Mode_CipherGeneratedIV()
            throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

        // Generate the encryption key
        byte[] keyHex = Hex.decode("000102030405060708090a0b0c0d0e0f");
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");

        // Create and AES cipher in Cipher Block Chaining (CBC) mode
        Cipher aesCipher = Cipher.getInstance("AES/CBC/NoPadding", "BC");

        // Hex encoded input data
        byte[] inputBytes = Hex.decode("a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7"+ "a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7");
        System.out.printf("input: %s%n", Hex.toHexString(inputBytes));

        // Setting the cipher mode to encrypt with the IV
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);

        // Generating a random IV, method 2
        byte[] iv = aesCipher.getIV();

        // Setting the cipher mode to encrypt with the IV
        byte[] output = aesCipher.doFinal(inputBytes);
        System.out.printf("encrypted: %s%n", Hex.toHexString(output));

        // Setting the cipher mode to decrypt with the IV
        aesCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
        System.out.printf("decrypted: %s%n", Hex.toHexString(aesCipher.doFinal(output)));
    }

    /**
     * The alternative to using the generated byte array returned by Cipher.getIV() is to use Cipher.getParameters()
     * which returns an AlgorithmParameters object. This can be used in the same way the IvParameterSpec
     * was in the last example
     * */
    @Test
    public void aesCipher_CipherBlockChaining_Mode_AlgorithmParametersIV()
            throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

        // Generate the encryption key
        byte[] keyHex = Hex.decode("000102030405060708090a0b0c0d0e0f");
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");

        // Create and AES cipher in Cipher Block Chaining (CBC) mode
        Cipher aesCipher = Cipher.getInstance("AES/CBC/NoPadding", "BC");

        // Hex encoded input data
        byte[] inputBytes = Hex.decode("a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7"+ "a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7");
        System.out.printf("input: %s%n", Hex.toHexString(inputBytes));

        // Setting the cipher mode to encrypt with the IV
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);

        // Generating a random IV, method 3
        AlgorithmParameters ivParams = aesCipher.getParameters();

        // Setting the cipher mode to encrypt with the IV
        byte[] output = aesCipher.doFinal(inputBytes);
        System.out.printf("encrypted: %s%n", Hex.toHexString(output));

        // Setting the cipher mode to decrypt with the IV
        aesCipher.init(Cipher.DECRYPT_MODE, secretKey, ivParams);
        System.out.printf("decrypted: %s%n", Hex.toHexString(aesCipher.doFinal(output)));
    }

    @Test
    public void aesCipher_CipherBlockChaining_Mode_PKCS5Padding()
            throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, ShortBufferException {

        // Generate the encryption key
        byte[] keyHex = Hex.decode("000102030405060708090a0b0c0d0e0f");
        SecretKey secretKey = new SecretKeySpec(keyHex, "AES");

        // Create and AES cipher in Cipher Block Chaining (CBC) mode
        Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");

        // Hex encoded input data
//        byte[] inputBytes = Hex.decode("a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7"+ "a0a1a2a3a4a5a6a7a0a1a2a3a4a5a6a7");
        byte[] inputBytes = IQRSContext.asciiToHex("This is a test").getBytes();
        System.out.printf("input: %s%n", Hex.toHexString(inputBytes));

        // Setting the cipher mode to encrypt with the IV
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);

        // Generating a random IV, method 2
        byte[] iv = aesCipher.getIV();
        byte[] output = aesCipher.doFinal(inputBytes);
        System.out.printf("encrypted: %s%n", Hex.toHexString(output));

        // Setting the cipher mode to decrypt with the IV
        aesCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));

        byte[] finalOutput = new byte[aesCipher.getOutputSize(output.length)];
        int len = aesCipher.update(output, 0, output.length, finalOutput, 0);
        len += aesCipher.doFinal(finalOutput, len);
        System.out.println("decrypted: "+ Hex.toHexString(Arrays.copyOfRange(finalOutput, 0, len)));
    }
}
