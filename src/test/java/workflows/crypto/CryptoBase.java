package workflows.crypto;

import com.google.zxing.qrcode.QRCodeWriter;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.converter.Converter;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.deserializer.BasicDeserializer;
import com.nexus.qrs.deserializer.Deserializer;
import com.nexus.qrs.serializer.BasicSerializer;
import com.nexus.qrs.serializer.Serializer;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CryptoBase {
    protected static QRCodeWriter qrCodeWriter;
    protected static Converter converter;
    protected static Serializer serializer;
    protected static Deserializer deserializer;
    protected static final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");
    protected static final Path outputSourcePath = Paths.get("src/test/resources/test-data/output/workflows");
    protected static Map<String, Path> inputFiles;

    protected static String[] payloadList;

    protected final IQRSContext IQRSContext = new IQRSContext() {};

    protected static SecureRandom random;

    static {
        inputFiles = Map.of(
                "helloWorld", inputSourcePath.resolve("text/helloWorld.txt"),
                "qrCodeUsage", inputSourcePath.resolve("text/qrCode-usage.txt"),
                "hotboxImage", inputSourcePath.resolve("binary/testImages/hotbox.jpg"),
                "quantumColaImage", inputSourcePath.resolve("binary/testImages/quantum-cola.jpg"),
                "abstractImage", inputSourcePath.resolve("binary/testImages/large/abstract.jpg"),
                "spaceImage", inputSourcePath.resolve("binary/testImages/large/space.png"),
                "linuxMascotImage", inputSourcePath.resolve("binary/testImages/linux-mascot.png"),
                "familyPgpFile", inputSourcePath.resolve("binary/my family (back in the day).pgp"),
                "keplerImage", inputSourcePath.resolve("binary/testImages/large/kepler-13ab-art.jpg")
        );
    }

    public static Path getInputFile(String file) {
        return inputFiles.get(file);
    }

    public CryptoBase() throws NoSuchAlgorithmException, NoSuchProviderException {
        converter = new SimpleConverter();
        serializer = new BasicSerializer();
        deserializer = new BasicDeserializer();
        qrCodeWriter = new QRCodeWriter();



        // Test data of QR codes
        payloadList = new String[14];
        payloadList[0] = "This is a test payload";
        payloadList[1] = "Optical Data Fountain!";
        payloadList[2] = "++++++++++++++++++++++";
        payloadList[3] = "----------------------";
        payloadList[4] = "0000000000000000000000";
        payloadList[5] = "1111111111111111111111";
        payloadList[6] = "1110001110001110001110";
        payloadList[7] = "0123456789012345678901";
        payloadList[8] = "++++++++++////////////";
        payloadList[9] = "//////////++++++++++++";
        payloadList[10] = "( ^.^)                ";
        payloadList[11] = "                (^.^ )";
        payloadList[12] = "                      ";
        payloadList[13] = " (> '.')> --> (X.X; ) ";

        // Setup Unlimited Strength Jurisdiction Policy Files
        Security.setProperty("crypto.policy", "unlimited");
        // Setup Encryption provider
        Security.addProvider(new BouncyCastleProvider());
        random = SecureRandom.getInstance("DEFAULT", "BC");
    }
}
