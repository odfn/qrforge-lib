package workflows.simple;

import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.converter.Converter;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.deserializer.BasicDeserializer;
import com.nexus.qrs.deserializer.Deserializer;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.QRData;
import com.nexus.qrs.serializer.BasicSerializer;
import com.nexus.qrs.serializer.Serializer;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class QRProcessing {
    private static QRCodeWriter qrCodeWriter;
    private static Converter converter;
    private static Serializer serializer;
    private static Deserializer deserializer;
    private static final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");
    private static final Path outputSourcePath = Paths.get("src/test/resources/test-data/output/workflows");
    private static List<File> inputFiles;

    private static String[] payloadList;

    private final IQRSContext IQRSContext = new IQRSContext() {};

    @BeforeAll
    public static void init() {
        converter = new SimpleConverter();
        serializer = new BasicSerializer();
        deserializer = new BasicDeserializer();
        qrCodeWriter = new QRCodeWriter();

        inputFiles = Arrays.asList(
                inputSourcePath.resolve("text/helloWorld.txt").toFile(),
                inputSourcePath.resolve("text/qrCode-usage.txt").toFile(),
                inputSourcePath.resolve("binary/testImages/hotbox.jpg").toFile(),
                inputSourcePath.resolve("binary/testImages/quantum-cola.jpg").toFile(),
                inputSourcePath.resolve("binary/testImages/large/abstract.jpg").toFile(),
                inputSourcePath.resolve("binary/testImages/large/kepler-13ab-art.jpg").toFile(),
                inputSourcePath.resolve("binary/testImages/large/space.png").toFile(),
                inputSourcePath.resolve("binary/testImages/linux-mascot.png").toFile(),
                inputSourcePath.resolve("binary/my family (back in the day).pgp").toFile()
        );

        // Test data of QR codes
        payloadList = new String[14];
        payloadList[0] = "This is a test payload";
        payloadList[1] = "Optical Data Fountain!";
        payloadList[2] = "++++++++++++++++++++++";
        payloadList[3] = "----------------------";
        payloadList[4] = "0000000000000000000000";
        payloadList[5] = "1111111111111111111111";
        payloadList[6] = "1110001110001110001110";
        payloadList[7] = "0123456789012345678901";
        payloadList[8] = "++++++++++////////////";
        payloadList[9] = "//////////++++++++++++";
        payloadList[10] = "( ^.^)                ";
        payloadList[11] = "                (^.^ )";
        payloadList[12] = "                      ";
        payloadList[13] = " (> '.')> --> (X.X; ) ";

    }

    @Test
    public void qrCodeWriterParams() throws WriterException, IOException, ByteStreamConverterNotFound {
        HashMap<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

        //NOTE: To limit the link capacity in the DataChain to allow for legible QR code generation.
        //NOTE: (linkCapacity = BlockCapacity - limiter)
        int limiter = 2000;

        FileDataChain dataChain = converter.readFile(inputFiles.get(0), limiter);
        Iterator<List<String>> iterator = dataChain.chainIterator();

        String[] input = {"test", ".txt", "This is a test."};
        Path tempFile = tempFileWriter.apply(input);

        FileDataChain tempDataChain = converter.readFile(tempFile.toFile(), limiter);
        Iterator<List<String>> tempIterator = tempDataChain.chainIterator();

        QRData qrCode = serializer.createQrCode(iterator.next(), 500, 500, hints);
        QRData strQrCode = serializer.createQrCode(tempIterator.next(), 500, 500, hints);

        converter.writeImageToFile(qrCode.getQrImage(), "png",
                outputSourcePath.resolve("QRProcessing/hotbox.png").toString());
        converter.writeImageToFile(strQrCode.getQrImage(), "png",
                outputSourcePath.resolve("QRProcessing/tempFile.png").toString());
    }

    @Test
    @Disabled
    public void converterComparison() throws IOException {

//        File file = TestUtils.loadTestFile("src/test/resources/test-data/input/binary/discord-0.0.27.deb");
//        assertNotNull(file);
//
//        try (InputStream is = Files.newInputStream(file.toPath());
//             InputStream is2 = Files.newInputStream(file.toPath())) {
//            ByteArrayInputStream bis1 = new ByteArrayInputStream(is.readAllBytes());
//            ByteArrayInputStream bis2 = new ByteArrayInputStream(is2.readAllBytes());
//            Instant start, end;
//
//            start = Instant.now();
//            FileDataChain dataChain1 = converter.readParallel(bis1, file.getName(), "txt");
//            end = Instant.now();
//            System.out.printf("Duration of converter.readParallel: %s\n", Duration.between(start, end));
//
//            start = Instant.now();
//            FileDataChain dataChain2 = converter.readFile(bis2, file.getName(),2000);
//            end = Instant.now();
//            System.out.printf("Duration of converter.readFile: %s\n", Duration.between(start, end));
//
//            assertNotNull(dataChain1);
//            assertNotNull(dataChain2);
//        } catch (ByteStreamConverterNotFound e) {
//            throw new RuntimeException(e);
//        }
    }

    @Test
    public void datachain_xor() throws IOException, WriterException, ByteStreamConverterNotFound {
        // OR Codes - for visualization
        List<QRData> qrCodes = new ArrayList<>();

        // QR Code dimensions
        int[] qrDims = {500, 500};

        // Test DataChain
        FileDataChain dc1 = converter.readFile(inputFiles.get(8), 2000);

        // Test DataChain iterator
        Iterator<List<String>> dc1Iter = dc1.chainIterator();
        List<List<String>> dcPayloads = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            List<String> next = dc1Iter.next();
            dcPayloads.add(next.subList(0, next.indexOf(":")));
        }

        List<String> xorEncodedPayload1 = payloadXOR(
                dcPayloads.get(0),
                dcPayloads.get(1),
                dcPayloads.get(2));

        List<String> xorEncodedPayload2 = payloadXOR(
                dcPayloads.get(3),
                dcPayloads.get(4),
                dcPayloads.get(5));
    }

//    @Test
//    @Disabled
//    public void xor_payload_multiplexing() throws IOException, WriterException {
//        // OR Codes - for visualization
//        List<QRData> qrCodes = new ArrayList<>();
//
//        // QR Code dimensions
//        int[] qrDims = {500, 500};
//
//        // Test DataChain payloads
//        List<String> dcPayloads = new ArrayList<>();
//        dcPayloads.add(converter.readHeaderString(payloadList[2], "test", 0).chainIterator().next());
//        dcPayloads.add(converter.readHeaderString(payloadList[13], "test", 0).chainIterator().next());
//        dcPayloads.add(converter.readHeaderString(payloadList[7], "test", 0).chainIterator().next());
//
//        // Test QR Codes for reference
//        qrCodes.add(serializer.createQrCode(dcPayloads.get(0), qrDims[0], qrDims[1]));
//        qrCodes.add(serializer.createQrCode(dcPayloads.get(1), qrDims[0], qrDims[1]));
//        qrCodes.add(serializer.createQrCode(dcPayloads.get(2), qrDims[0], qrDims[1]));
//
//        // XOR encode operation on QR payloads
//        List<String> xorEncodedQrPayload = payloadXOR(
//                dcPayloads.get(0),
//                dcPayloads.get(1),
//                dcPayloads.get(2));
//
//        // To retrieve a block from an encoded block, XOR the encoded block by the other component blocks
//        // to compute the missing block
//        List<String> decodeResult1 = payloadXOR(xorEncodedQrPayload, dcPayloads.get(1), dcPayloads.get(2));
//        List<String> decodeResult2 = payloadXOR(xorEncodedQrPayload, dcPayloads.get(0), dcPayloads.get(2));
//        List<String> decodeResult3 = payloadXOR(xorEncodedQrPayload, dcPayloads.get(0), dcPayloads.get(1));
//
//        // XOR encoded QR Code for reference
//        qrCodes.add(serializer.createQrCode(xorEncodedQrPayload, qrDims[0], qrDims[1]));
//        assertNotNull(qrCodes);
//
//        // Converting QR payloads from hex to ascii (raw data)
//        String payload1Ascii = converter.hexToAscii(String.join("", dcPayloads.get(0)));
//        String payload2Ascii = converter.hexToAscii(String.join("", dcPayloads.get(1)));
//        String payload3Ascii = converter.hexToAscii(String.join("", dcPayloads.get(2)));
//        String xorAscii = converter.hexToAscii(String.join("", xorEncodedQrPayload));
//
//        String decodeResult1Ascii = converter.hexToAscii(String.join("", decodeResult1));
//        String decodeResult2Ascii = converter.hexToAscii(String.join("", decodeResult2));
//        String decodeResult3Ascii = converter.hexToAscii(String.join("", decodeResult3));
//
//        assertEquals(payload1Ascii, decodeResult1Ascii);
//        assertEquals(payload2Ascii, decodeResult2Ascii);
//        assertEquals(payload3Ascii, decodeResult3Ascii);
//    }

    public void assertSame(List<String> left, List<String> right) {
        for (int i = 0; i < left.size(); i++) {
            String leftValue = left.get(i);
            String rightValue = right.get(i);
            if (!leftValue.equalsIgnoreCase(rightValue))
                throw new AssertionFailedError("Values are different", leftValue, rightValue);
        }
    }

    Function<int[], List<Integer>> filterNonEmpty = (p) ->
            Arrays.stream(p)
             .filter(i -> i != 0)
             .boxed()
             .collect(Collectors.toList());

    BiFunction<int[], int[], int[]> array_xor = (int[] left, int[] right) -> {
        int[] xorBits = new int[left.length];
        for (int x = 0; x < left.length - 1; x++) {
            xorBits[x] = left[x] ^ right[x];
        }
        return xorBits;
    };

    //------------------------------QRSContext utility methods start----------------------------------------------------
    BiFunction<List<Integer>, List<Integer>, List<Integer>> arrayListXOR = (List<Integer> left, List<Integer> right) -> {
        List<Integer> xorBits = new ArrayList<>();
        for (int x = 0; x < left.size(); x++) {
            xorBits.add((left.get(x) ^ right.get(x)));
        }
        return xorBits;
    };

    Function<List<String>, List<Integer>> hex2decimal = (List<String> hex) ->
            hex.stream()
            .map(converter::hexToDec)
            .map(Integer::parseInt)
            .collect(Collectors.toList());

    @SafeVarargs
    public final List<String> payloadXOR(List<String>... payloads) {
        if (payloads.length == 1)
            return payloads[0];

        // Hex to Decimal conversion of QR payloads
        List<List<Integer>> _payloads = new ArrayList<>();
        for(List<String> payload: payloads) {
            _payloads.add(hex2decimal.apply(payload));
        }

        Iterator<List<Integer>> payloadIter = _payloads.iterator();
        List<Integer> accumulator = new ArrayList<>();
        while (payloadIter.hasNext()) {
            List<Integer> p1 = payloadIter.next();
            List<Integer> p2 = payloadIter.next();
            accumulator = arrayListXOR.apply(p1, p2);

            while (payloadIter.hasNext()) {
                accumulator = arrayListXOR.apply(accumulator, payloadIter.next());
            }
        }
        return accumulator.stream().map(converter::toHex).collect(Collectors.toList());
    }
    //------------------------------QRSContext utility methods end------------------------------------------------------

    Function<String[], Path> tempFileWriter = (params) -> {
        Path tempFile = null;
        try {
            tempFile = Files.createTempFile(params[0], params[1]);
            Files.write(tempFile, params[2].getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tempFile;
    };
}
