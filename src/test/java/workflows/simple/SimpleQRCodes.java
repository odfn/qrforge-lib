package workflows.simple;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.converter.Converter;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.deserializer.BasicDeserializer;
import com.nexus.qrs.deserializer.Deserializer;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.QRData;
import com.nexus.qrs.serializer.BasicSerializer;
import com.nexus.qrs.serializer.Serializer;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SimpleQRCodes {
    private static Converter converter;
    private static Serializer serializer;
    private static Deserializer deserializer;
    private final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");
    private final Path outputSourcePath = Paths.get("src/test/resources/test-data/output/workflows");

    private final IQRSContext IQRSContext = new IQRSContext() {};

    private static ByteArrayOutputStream outputStream;

    @BeforeAll
    public static void init() {
        converter = new SimpleConverter();
        serializer = new BasicSerializer();
        deserializer = new BasicDeserializer();
    }

    @Test
    @Order(1)
    public void processInputFiles() {
        // Select file(s) to process
        List<File> inputFiles = Arrays.asList(
                TestUtils.getInputFile("sample1Video").toFile()
        );

        List<Thread> threads = new ArrayList<>();

        inputFiles.forEach(inputFile -> {
            Processor processor = new Processor(inputFile);
            String threadName = inputFile.getName().split("\\.")[0];
            Thread t = new Thread(processor, threadName);
            threads.add(t);
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        // Output should result with QR codes created in the following directories...
        List<Path> outputPaths = Arrays.asList(
//                outputSourcePath.resolve("hotbox"),
//                outputSourcePath.resolve("helloWorld"),
//                outputSourcePath.resolve("qrCode-usage"),
//                outputSourcePath.resolve("quantum-cola"),
//                outputSourcePath.resolve("abstract"),
//                outputSourcePath.resolve("kepler-13ab-art"),
//                outputSourcePath.resolve("space"),
//                outputSourcePath.resolve("linux-mascot"),
                outputSourcePath.resolve("sample-1")
        );


        outputPaths.forEach(p -> assertTrue(Files.isDirectory(p), p.toString()));
    }

    @Test
    @Order(2)
    public void processQRCodes() {
        List<Path> inputQRFilePaths = Arrays.asList(
                //NOTE: These seem to work...
//                outputSourcePath.resolve("helloWorld"),
//                outputSourcePath.resolve("qrCode-usage"),
//                outputSourcePath.resolve("linux-mascot"),
                outputSourcePath.resolve("sample-1")
//                outputSourcePath.resolve("my family (back in the day)"),

                //FIXME: Figure out why these QR codes are failing
//                outputSourcePath.resolve("hotbox"),
//                outputSourcePath.resolve("quantum-cola"),
//                outputSourcePath.resolve("abstract"),
//                outputSourcePath.resolve("kepler-13ab-art"),
//                outputSourcePath.resolve("space")
        );
        Map<String, String> pathsToExtensions = new HashMap<>();

        //FIXME: JPGs seem to fail....Consider a workaround to convert
        // JPGs to PNGs
        pathsToExtensions.put("hotbox", "jpg");
        pathsToExtensions.put("quantum-cola", "jpg");
        pathsToExtensions.put("abstract", "jpg");
        pathsToExtensions.put("kepler-13ab-art", "jpg");
        pathsToExtensions.put("space", "png");

        //NOTE: These seem to work...
        pathsToExtensions.put("helloWorld", "txt");
        pathsToExtensions.put("qrCode-usage", "txt");
        pathsToExtensions.put("linux-mascot", "png");
        pathsToExtensions.put("sample-1", "mp4");

        //FIXME: Figure out why this failed
//        pathsToExtensions.put("my family (back in the day)", "pgp");

        assertNotNull(inputQRFilePaths);

        for (Path inputPath: inputQRFilePaths) {
            //TODO: Ensure the new filename is used instead of the QR code filename
            String newFileName = String.join(".",
                    inputPath.getFileName().toString(),
                    pathsToExtensions.get(inputPath.getFileName().toString()));
            List<File> files;
            try {
                //NOTE: These two methods may be deprecated
                files = IQRSContext.getFiles(inputPath);
                IQRSContext.sortQrFiles(files); //FIXME: improve sorting strategy for large filesets

                Matcher matcher = IQRSContext.getExtensionPatterns().matcher(newFileName);
                String extension = matcher.find() ? matcher.group(1) :  "";
                List<List<String>> partitionData = new ArrayList<>();
                files.stream()
                        .filter(file -> !file.getAbsoluteFile().getName().contains(newFileName))
                        .forEach(file -> {
                    try {
                        String codeData = deserializer.readQrCode(file);
                        List<String> partition = IQRSContext.partition(IQRSContext.stripSequenceCounter(codeData), extension);
                        partitionData.add(partition);
                    } catch (IOException | NotFoundException | FormatException e) {
                        if (e instanceof IOException) {
                            e.printStackTrace();
                        } else if (e instanceof NotFoundException) {
                            System.err.printf("Error: %s%n", file);
                        }
                    }
                });
                converter.writeQrCodesToFile(partitionData, inputPath.resolve(newFileName).toString());
            } catch (IOException | ByteStreamConverterNotFound e) {
                e.printStackTrace();
            }
        }
    }

    class Processor implements Runnable {

        private File inputFile;
        private Logger logger;
        private StringBuilder logSb = new StringBuilder();

        public Processor(File inputFile) {
            this.inputFile = inputFile;
            String qrFileName = inputFile.getName().split("\\.")[0];
            this.logger = Logger.getLogger(qrFileName);

        }

        Function<String, Void> log_info = (message) -> {
            logSb.append(message);
            logger.info(logSb.toString());
            logSb.delete(0, logSb.length());
            return null;
        };

        @Override
        public void run() {
            int[] codeDims = {200, 200};
            String qrFileName = inputFile.getName().split("\\.")[0];
            Path outputPath = outputSourcePath.resolve(qrFileName);

            try {
                // Convert file to a DataChain
                //NOTE: A limit of 2000 seems to work fine for the files. Consider more research in this...
                FileDataChain dataChain = converter.readFile(inputFile, 2000);
                log_info.apply(String.format("Created %s datachain of length %d%n", qrFileName, dataChain.blockCount()));

                AtomicInteger count = new AtomicInteger();
                if (!Files.exists(outputPath)) {
                    log_info.apply(String.format("Creating directory for %s%n", outputPath.getFileName()));
                    Files.createDirectories(outputPath);
                }

                // Write QR codes to outPath directory
                List<QRData> qrCodes = serializer.createQrCodes(dataChain.getContents(), codeDims[0], codeDims[1]);
                for (QRData qrCode : qrCodes) {
                    // Create filename for QR code
                    String outputFileName = String.format("%s-QR-%d.png", qrFileName, count.getAndIncrement());
                    String outPath = outputPath.resolve(outputFileName).toString();
                    converter.writeImageToFile(qrCode.getQrImage(), "PNG", outPath);
                    log_info.apply(String.format("Wrote %s to location: %s%n", outputFileName, outPath));
                }
                log_info.apply(String.format("Wrote %d QR codes to location %s%n", count.get(), outputPath));

            } catch (ByteStreamConverterNotFound | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
