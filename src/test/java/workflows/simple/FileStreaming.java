package workflows.simple;

import org.junit.jupiter.api.Test;
import workflows.crypto.CryptoBase;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class FileStreaming extends CryptoBase {
    public FileStreaming() throws NoSuchAlgorithmException, NoSuchProviderException {
    }

    //TODO: Improve file input streaming to reduce processing time
    @Test
    public void fileStreaming() throws IOException {
        Path fp = getInputFile("keplerImage");

        try (InputStream is = Files.newInputStream(fp)) {
            byte[] buffer = new byte[is.available()];
            is.read(buffer);


        }
    }
}
