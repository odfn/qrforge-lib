package workflows;

import static org.junit.jupiter.api.Assertions.*;

import com.nexus.qrs.converter.Converter;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class FileDataChainSortingTest {

    private final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");
    private static Converter converter;

    @BeforeAll
    public static void init() {
        converter = new SimpleConverter();
    }

    @Test
    public void testFileDataChainSorting() throws ByteStreamConverterNotFound, IOException {
        Path filePath = inputSourcePath.resolve("binary/testImages/quantum-cola.png");
        assertNotNull(filePath);
        Instant start, end;

        start = Instant.now();
        FileDataChain fileDataChain = converter.readFile(filePath.toFile(), 2000);
        end = Instant.now();
        System.out.println("fileDataChain duration: " + Duration.between(start, end));

        // Step 3: Retrieve the link data
        List<List<String>> linkData = fileDataChain.getContents();
        assertNotNull(linkData, "Link data should not be null");

        // Step 4: Randomly shuffle the link data
        Collections.shuffle(linkData, new Random());

        // Step 5: Implement and apply Heap Sort to the link data
        start = Instant.now();
        heapSort(linkData, Comparator.comparingInt(List::size)); // Example: Sorting based on list size
        end = Instant.now();
        System.out.println("heapSort duration: " + Duration.between(start, end));

        // Verify the sorting (Heap Sort)
        for (int i = 1; i < linkData.size(); i++) {
            assertTrue(linkData.get(i-1).size() <= linkData.get(i).size(), "HeapSort failed to correctly sort the link data");
        }

        // Step 6: Randomly shuffle the link data again
        Collections.shuffle(linkData, new Random());

        // Step 7: Implement and apply Dual-Pivot Quick Sort to the link data
        start = Instant.now();
        dualPivotQuickSort(linkData, 0, linkData.size() - 1, Comparator.comparingInt(List::size));
        end = Instant.now();
        System.out.println("dualPivotQuickSort duration: " + Duration.between(start, end));

        // Verify the sorting (Dual-Pivot Quick Sort)
        for (int i = 1; i < linkData.size(); i++) {
            assertTrue(linkData.get(i-1).size() <= linkData.get(i).size(), "Dual-Pivot Quick Sort failed to correctly sort the link data");
        }
    }

    private <T> void heapSort(List<T> list, Comparator<T> comparator) {
        int size = list.size();

        // Build heap (rearrange array)
        for (int i = size / 2 - 1; i >= 0; i--)
            heapify(list, size, i, comparator);

        // One by one extract an element from heap
        for (int i = size - 1; i >= 0; i--) {
            // Move current root to end
            Collections.swap(list, 0, i);

            // call max heapify on the reduced heap
            heapify(list, i, 0, comparator);
        }
    }

    private <T> void heapify(List<T> list, int heapSize, int rootIndex, Comparator<T> comparator) {
        int largest = rootIndex; // Initialize largest as root
        int left = 2 * rootIndex + 1; // left = 2*i + 1
        int right = 2 * rootIndex + 2; // right = 2*i + 2

        // If left child is larger than root
        if (left < heapSize && comparator.compare(list.get(left), list.get(largest)) > 0)
            largest = left;

        // If right child is larger than largest so far
        if (right < heapSize && comparator.compare(list.get(right), list.get(largest)) > 0)
            largest = right;

        // If largest is not root
        if (largest != rootIndex) {
            Collections.swap(list, rootIndex, largest);

            // Recursively heapify the affected sub-tree
            heapify(list, heapSize, largest, comparator);
        }
    }

    private <T> void dualPivotQuickSort(List<T> list, int low, int high, Comparator<T> comparator) {
        if (low < high) {
            // lp means left pivot, and rp means right pivot.
            int[] pivots = partition(list, low, high, comparator);
            int lp = pivots[0], rp = pivots[1];

            dualPivotQuickSort(list, low, lp - 1, comparator);
            dualPivotQuickSort(list, lp + 1, rp - 1, comparator);
            dualPivotQuickSort(list, rp + 1, high, comparator);
        }
    }

    private <T> int[] partition(List<T> list, int low, int high, Comparator<T> comparator) {
        if (comparator.compare(list.get(low), list.get(high)) > 0)
            Collections.swap(list, low, high);

        T lpivot = list.get(low), rpivot = list.get(high);

        int i = low + 1;
        int lt = low + 1;
        int gt = high - 1;

        while (i <= gt) {
            if (comparator.compare(list.get(i), lpivot) < 0) {
                Collections.swap(list, i++, lt++);
            } else if (comparator.compare(list.get(i), rpivot) > 0) {
                Collections.swap(list, i, gt--);
            } else {
                i++;
            }
        }

        Collections.swap(list, low, --lt);
        Collections.swap(list, gt + 1, high);

        return new int[] { lt, gt + 1 };
    }
}