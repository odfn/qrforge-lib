package com.nexus.qrs.serializer.image;

import com.google.zxing.WriterException;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.QRData;
import com.nexus.qrs.serializer.BasicSerializer;
import com.nexus.qrs.serializer.Serializer;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ImageSerializerTest {

    private static Serializer serializer;
    private static FileDataChain fileDataChain;

    @BeforeAll
    public static void init() throws ByteStreamConverterNotFound, IOException {
        serializer = new BasicSerializer();
    }

    @Test
    public void createQrCodes_fromImage() throws IOException {

        int[] codeDims = {400, 400};
        File fileData = TestUtils.getInputFile("linuxMascotImage").toFile();
        fileDataChain = new SimpleConverter().readFile(fileData, 2000);
        assertNotNull(fileDataChain);
        List<QRData> qrCodes = serializer.createQrCodes(fileDataChain.getContents(), codeDims[0], codeDims[1]);;
        AtomicInteger count = new AtomicInteger(0);
        for (QRData qrCode: qrCodes) {
            assertNotNull(qrCode);
            assertNotNull(qrCode.getQrImage());
            assertEquals(codeDims[0], qrCode.getQrImage().getWidth());
            assertEquals(codeDims[1], qrCode.getQrImage().getHeight());

            String qrCodePath = String.format(
                    "src/test/resources/test-data/output/qr/multiple/linuxMascot/linux-mascot-QR-%d.png",
                    count.getAndIncrement());
            TestUtils.writeQrCodeToFile(qrCode.getQrImage(), "PNG", qrCodePath);
        }
    }
}
