package com.nexus.qrs.serializer;

import com.google.zxing.WriterException;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.QRData;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SerializerTest {

    private static Serializer serializer;

    @BeforeAll
    public static void init() {
        serializer = new BasicSerializer();
    }

    @Test
    public void createQrCode_fromSimpleText() throws WriterException, IOException {
        // 'Hello, world!' in octal
        String octalTestData = "110 145 154 154 157 054 040 167 157 162 154 144 041";
        List<String> testInput = Arrays.asList(octalTestData.split(" "));
        assertNotNull(testInput);

        QRData qrCode = serializer.createQrCode(testInput, 200, 200);
        assertNotNull(qrCode);
        assertNotNull(qrCode.getQrImage());
        assertEquals(200, qrCode.getQrImage().getWidth());
        assertEquals(200, qrCode.getQrImage().getHeight());
        TestUtils.writeQrCodeToFile(qrCode.getQrImage(), "PNG", "src/test/resources/test-data/output/qr/hello-world-QR.png");
    }

    @Test
    public void createQrCode_fromLargeText() throws WriterException, IOException {
        String octalTestData = TestUtils.readTestFile("src/test/resources/test-data/input/text/qrCode-usage-octal-2.txt");
        List<String> testInput = Arrays.asList(octalTestData.split(" "));
        assertNotNull(testInput);

        QRData qrCode = serializer.createQrCode(
                testInput,
                400, 400);
        assertNotNull(qrCode);
        assertEquals(400, qrCode.getQrImage().getWidth());
        assertEquals(400, qrCode.getQrImage().getHeight());
        TestUtils.writeQrCodeToFile(qrCode.getQrImage(), "PNG", "src/test/resources/test-data/output/qr/testFile-octal-QR.png");
    }

    @Test
    public void createQrCodes_fromImage() throws IOException {
        File fileData = TestUtils.getInputFile("linuxMascotImage").toFile();
        Instant dcStart, dcEnd;
        dcStart = Instant.now();
        FileDataChain fileDataChain = new SimpleConverter().readFile(fileData, 2000);
        dcEnd = Instant.now();
        Logger.getLogger(SerializerTest.class.getName()).info("converter.readEncryptedString() duration: " + Duration.between(dcStart, dcEnd));

        int[] codeDims = {400, 400};
        assertNotNull(fileDataChain);
        List<List<String>> chainData = fileDataChain.getContents();
        AtomicInteger count = new AtomicInteger(0);

        dcStart = Instant.now();
        List<QRData> qrCodes = serializer.createQrCodes(chainData, codeDims[0], codeDims[1]);
        dcEnd = Instant.now();
        Logger.getLogger(SerializerTest.class.getName()).info("serializer.createQrCodes() duration: " + Duration.between(dcStart, dcEnd));

        assertNotNull(qrCodes);

        qrCodes.forEach(qrCode -> {
            assertNotNull(qrCode);
            assertNotNull(qrCode.getQrImage());
            assertEquals(codeDims[0], qrCode.getQrImage().getWidth());
            assertEquals(codeDims[1], qrCode.getQrImage().getHeight());

            String qrCodePath = String.format(
                    "src/test/resources/test-data/output/qr/multiple/linuxMascot/linux-mascot-QR-%d.png",
                    count.getAndIncrement());
            try {
                TestUtils.writeQrCodeToFile(qrCode.getQrImage(), "PNG", qrCodePath);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

    }
}
