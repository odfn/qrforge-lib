package com.nexus.qrs.converter;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.deserializer.BasicDeserializer;
import com.nexus.qrs.deserializer.Deserializer;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.HeaderDataChain;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ConverterTest {

    private static Converter converter;
    private static Deserializer deserializer;

    private static IQRSContext IQRSContext;
    private final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");

    @BeforeAll
    public static void init() {
        converter = new SimpleConverter();
        deserializer = new BasicDeserializer();
        IQRSContext = new IQRSContext() {};
    }

    @Test
    void parseString_ToDataChain() {
        String[] payload = {
                "This is a test payloadThis is a test payload",
                "Optical Data Fountain!Optical Data Fountain!"
        };

        HeaderDataChain textChain = converter.readHeaderString(payload[0], "Text DataChain 1", 2000);
        assertNotNull(textChain);
        assertEquals(payload[0].length(), textChain.fileSize());
    }

    @Test
    public void convertReadVideoFileTest() throws IOException, ByteStreamConverterNotFound {
        Path filePath = inputSourcePath.resolve("video/sample-1.mp4");
        assertNotNull(filePath);
        Instant start, end;

        start = Instant.now();
        FileDataChain testFileChain = converter.readFile(filePath.toFile(), 2000);
        end = Instant.now();

        System.out.println("parallelReadVideoFile() duration: " + Duration.between(start, end));
        assertNotNull(testFileChain);
        assertEquals(19708, testFileChain.getChainSize());
    }

    @Test
    public void convertReadSmallImageFileTest() throws IOException, ByteStreamConverterNotFound {
        Path filePath = inputSourcePath.resolve("binary/testImages/hotbox.png");
        assertNotNull(filePath);
        Instant start, end;

        start = Instant.now();
        FileDataChain testFileChain = converter.readFile(filePath.toFile(), 2000);
        end = Instant.now();

        System.out.println("parallelReadVideoFile() duration: " + Duration.between(start, end));
        assertNotNull(testFileChain);
        assertEquals(6971, testFileChain.getChainSize());
    }

    @Test
    public void convertReadJarFileTest() throws IOException, ByteStreamConverterNotFound {
        Path filePath = inputSourcePath.resolve("binary/qr-forge-0.0.1-SNAPSHOT.jar");
        assertNotNull(filePath);
        Instant start, end;

        start = Instant.now();
        FileDataChain testFileChain = converter.readFile(filePath.toFile(), 2000);
        end = Instant.now();

        System.out.println("parallelReadVideoFile() duration: " + Duration.between(start, end));
        assertNotNull(testFileChain);
        assertEquals(311573, testFileChain.getChainSize());
    }

    @Test
    public void convertReadSmallImageInputStreamTest() throws IOException, ByteStreamConverterNotFound {
        Path filePath = inputSourcePath.resolve("binary/testImages/quantum-cola.png");
        assertNotNull(filePath);
        InputStream is = new FileInputStream(filePath.toFile());

        Instant start, end;
        start = Instant.now();
        FileDataChain testFileChain = converter.readFile(is, "quantum-cola.png", 2000);
        end = Instant.now();

        System.out.println("convertReadSmallImageInputStreamTest() duration: " + Duration.between(start, end));
        assertNotNull(testFileChain);
        assertEquals(2282, testFileChain.getChainSize());
    }

    @Test
    public void convertReadLargeImageFileTest() throws IOException, ByteStreamConverterNotFound {
        Path filePath = inputSourcePath.resolve("binary/testImages/large/space.png");
        assertNotNull(filePath);
        Instant start, end;

        start = Instant.now();
        FileDataChain testFileChain = converter.readFile(filePath.toFile(), 2000);
        end = Instant.now();

        System.out.println("convertReadLargeImageFileTest() duration: " + Duration.between(start, end));
        assertNotNull(testFileChain);
        assertEquals(15282, testFileChain.getChainSize());
    }

    @Test
    void parseSimpleTextFile_andWrite_NewFile() throws ByteStreamConverterNotFound, IOException {

        File file = TestUtils.loadTestFile("src/test/resources/test-data/input/text/simple.txt");
        assertNotNull(file);

        FileDataChain dataChain = converter.readFile(file, 2000);
        assertNotNull(dataChain);

        List<String> out = dataChain.chainIterator().next();
        File outFile = converter.writeFile(out.subList(0, out.indexOf("|")), "src/test/resources/test-data/output/text/simple-out.txt");
        assertNotNull(outFile);
        assertTrue(Files.exists(outFile.toPath()));
    }

    @Test
    public void writeSimpleTextQrCodeBundle_ToFile() throws IOException, ByteStreamConverterNotFound {
        List<File> qrFiles = converter.getFiles(Paths.get("src/test/resources/test-data/output/qr/multiple/qrCodeUsage"));
        converter.sortQrFiles(qrFiles);

        List<List<String>> fileData = new ArrayList<>();
        qrFiles.forEach(qrFile -> {
            try {
                String qrCodeData = deserializer.readQrCode(qrFile);
                assertNotNull(qrCodeData);

                //NOTE: These QR codes are known to have text
                fileData.add(IQRSContext.partition(IQRSContext.stripSequenceCounter(qrCodeData), "txt"));
            } catch (IOException | NotFoundException e) {
                e.printStackTrace();
            } catch (FormatException e) {
                throw new RuntimeException(e);
            }
        });
        assertNotNull(fileData);
        assertEquals(100, fileData.size());
        assertNotNull(converter.writeQrCodesToFile(fileData, "src/test/resources/test-data/output/text/qrCode-usage-out.txt"));
    }
}
