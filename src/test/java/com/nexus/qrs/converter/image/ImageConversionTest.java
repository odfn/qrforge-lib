package com.nexus.qrs.converter.image;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.converter.Converter;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.deserializer.BasicDeserializer;
import com.nexus.qrs.deserializer.Deserializer;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ImageConversionTest {

    private static Converter converter;
    private static Deserializer deserializer;
    private static String imagePath;
    private static IQRSContext IQRSContext = new IQRSContext() {};
    private static File fileData;

    @BeforeAll
    public static void init() {
        converter = new SimpleConverter();
        deserializer = new BasicDeserializer();
        imagePath = "src/test/resources/test-data/input/binary/testImages/linux-mascot.png";
        fileData = TestUtils.loadTestFile("src/test/resources/test-data/input/binary/testImages/linuxMascot-filedata.txt");
    }

    @Test
    @Deprecated
    public void parseImage_ToOctal() throws ByteStreamConverterNotFound, IOException {
        File imageFile = Paths.get(imagePath).toFile();
        assertNotNull(imageFile);

        FileDataChain fileDataChain = converter.readFile(fileData, 2000);
        FileDataChain imageDataChain = converter.readFile(imageFile, 2000);
        assertNotNull(fileDataChain);
        assertNotNull(imageDataChain);
    }

    @Test
    public void writeSimpleImageQrCodeBundle_ToFile() throws IOException, ByteStreamConverterNotFound {
        List<File> qrFiles = converter.getFiles(Paths.get("src/test/resources/test-data/output/qr/multiple/linuxMascot"));
        converter.sortQrFiles(qrFiles);

        List<List<String>> fileData = new ArrayList<>();
        qrFiles.forEach(qrFile -> {
            try {
                String qrCodeData = deserializer.readQrCode(qrFile);
                assertNotNull(qrCodeData);

                Matcher matcher = IQRSContext.getExtensionPatterns().matcher(qrFile.getName());
                String extension = matcher.find() ? matcher.group(1) : "";

                fileData.add(IQRSContext.partition(IQRSContext.stripSequenceCounter(qrCodeData), extension));
            } catch (IOException | NotFoundException e) {
                e.printStackTrace();
            } catch (FormatException e) {
                throw new RuntimeException(e);
            }
        });
        assertNotNull(fileData);
        assertEquals(141, fileData.size());
        converter.writeQrCodesToFile(fileData, "src/test/resources/test-data/output/binary/testImages/linuxMascot-out.png");
    }
}
