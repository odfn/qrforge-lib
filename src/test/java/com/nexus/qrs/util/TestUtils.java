package com.nexus.qrs.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class TestUtils {

    protected static final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");
    protected static final Path outputSourcePath = Paths.get("src/test/resources/test-data/output/workflows");
    protected static Map<String, Path> inputFiles;

    static {
        inputFiles = Map.of(
                "helloWorld", inputSourcePath.resolve("text/helloWorld.txt"),
                "qrCodeUsage", inputSourcePath.resolve("text/qrCode-usage.txt"),
                "hotboxImage", inputSourcePath.resolve("binary/testImages/hotbox.jpg"),
                "quantumColaImage", inputSourcePath.resolve("binary/testImages/quantum-cola.jpg"),
                "abstractImage", inputSourcePath.resolve("binary/testImages/large/abstract.jpg"),
                "spaceImage", inputSourcePath.resolve("binary/testImages/large/space.png"),
                "linuxMascotImage", inputSourcePath.resolve("binary/testImages/linux-mascot.png"),
                "familyPgpFile", inputSourcePath.resolve("binary/my family (back in the day).pgp"),
                "keplerImage", inputSourcePath.resolve("binary/testImages/large/kepler-13ab-art.jpg"),
                "sample1Video", inputSourcePath.resolve("video/sample-1.mp4")
        );
    }

    public static Path getInputFile(String file) {
        return inputFiles.get(file);
    }

    public static File loadTestFile(String fileLocation) {
        return Paths.get(fileLocation).toFile();
    }

    public static String readTestFile(String filePath) {
        Path fp = Paths.get(filePath);
        StringBuilder sb = new StringBuilder();
        try (InputStream inputStream = Files.newInputStream(fp);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String value;
            while ((value = reader.readLine()) != null) {
                sb.append(value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static void writeQrCodeToFile(BufferedImage qrCode, String fileFormat, String qrPath) throws IOException {
        ImageIO.write(qrCode, fileFormat, Paths.get(qrPath).toFile());
    }
}
