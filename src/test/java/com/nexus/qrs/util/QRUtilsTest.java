package com.nexus.qrs.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexus.qrs.model.FileDataChain;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class QRUtilsTest {

    private final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");
    private final Path outputSourcePath = Paths.get("src/test/resources/test-data/output");
    private static ObjectMapper mapper;

    @BeforeAll
    public static void init(){
        mapper = new ObjectMapper();
    }

    @Test
    @Disabled
    public void dataChainArchiveTest() throws IOException {
        Path filePath = inputSourcePath.resolve("json/sample-1.mp4.json");
        Path compressedPath = outputSourcePath.resolve("compressed/sample-1.mp4.7z");

        FileDataChain linuxMascotChain = mapper.readValue(filePath.toFile(), FileDataChain.class);
        assertNotNull(linuxMascotChain);

        Path result = QRUtils.archiveChainData(linuxMascotChain, compressedPath);
        assertNotNull(result);
        assertTrue(result.toFile().exists());
    }
}
