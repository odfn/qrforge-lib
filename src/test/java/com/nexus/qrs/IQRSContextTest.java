package com.nexus.qrs;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class IQRSContextTest {
    private static final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");
    private final Path outputSourcePath = Paths.get("src/test/resources/test-data/output/");
    private static List<File> inputFiles;
    IQRSContext IQRSContext = new IQRSContext() {};

    @BeforeAll
    public static void init() {
        inputFiles = Arrays.asList(
                inputSourcePath.resolve("binary/testImages/hotbox.jpg").toFile(),
                inputSourcePath.resolve("binary/testImages/quantum-cola.jpg").toFile()
        );
    }

    @Test
    public void JPEG_to_PNG_fileConversionTest() {
        inputFiles.forEach(file -> IQRSContext.jpegToPng(file));
    }

    @Test
    public void PNG_to_JPEG_fileConversionTest() {

    }
}
