package com.nexus.qrs.model;

import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.converter.bytestream.ByteStreamConverter;
import com.nexus.qrs.converter.bytestream.ByteStreamConverterFactory;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class DataChainTest {

    private final IQRSContext IQRSContext = new IQRSContext() {};
    private final Path inputSourcePath = Paths.get("src/test/resources/test-data/input");

    @Test
    public void smallFileTest() {
        FileDataChain fileDataChain = new FileDataChain("helloWorld.txt");

        // Load the data as string and byte arrays
        String inputPath = inputSourcePath.resolve("text/helloWorld.txt").toString();
        String fileData = TestUtils.readTestFile(inputPath);
        fileDataChain.addData(fileData, ByteStreamConverterFactory.getDefaultConverter());

        assertNotNull(fileDataChain);
        assertEquals(1, fileDataChain.blockCount());
        List<String> fileChainLinkData = fileDataChain.chainIterator().next();
        assertEquals(148, fileChainLinkData.size());
    }

    @Test
    public void largeFileTest() {
        FileDataChain smallChain = new FileDataChain("qrCode.txt");
        FileDataChain largeChain = new FileDataChain("qrCode.txt");

        final int smallChainLinkCapacity = 148;
        final int largeChainLinkSize = 2363;

        String smallFileData = TestUtils.readTestFile(inputSourcePath.resolve("text/qrCode.txt").toString());
        assertNotNull(smallFileData);

        String largeFileData = TestUtils.readTestFile(inputSourcePath.resolve("text/qrCode-usage.txt").toString());
        assertNotNull(largeFileData);

        smallChain.addData(smallFileData, ByteStreamConverterFactory.getDefaultConverter());
        largeChain.addData(largeFileData, ByteStreamConverterFactory.getDefaultConverter());

        assertNotNull(smallChain);
        assertEquals(4, smallChain.blockCount());
        List<String> chainLink = smallChain.chainIterator().next();
        assertEquals(smallChainLinkCapacity, chainLink.size());

        assertNotNull(largeChain);
        assertEquals(99, largeChain.blockCount());
        Iterator<List<String>> chainIter = largeChain.chainIterator();
        chainIter.forEachRemaining(element -> {
            assertNotNull(element);
            assertTrue(element.size() <= largeChainLinkSize);
        });
    }

    @Test
    public void largeFile_withLimiterTest() {
        FileDataChain largeChain = new FileDataChain("qrCode-usage.txt", 2000);
        final int largeChainLinkSize = 2363;

        String largeFileData = TestUtils.readTestFile(inputSourcePath.resolve("text/qrCode-usage.txt").toString());
        assertNotNull(largeFileData);

        largeChain.addData(largeFileData, ByteStreamConverterFactory.getDefaultConverter());

        assertNotNull(largeChain);
        assertEquals(99, largeChain.blockCount());
        Iterator<List<String>> chainIter = largeChain.chainIterator();
        chainIter.forEachRemaining(element -> {
            assertNotNull(element);
            assertTrue(element.size() <= largeChainLinkSize);
        });
    }
}
