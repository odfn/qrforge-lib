package com.nexus.qrs;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.nexus.qrs.converter.Converter;
import com.nexus.qrs.converter.SimpleConverter;
import com.nexus.qrs.deserializer.BasicDeserializer;
import com.nexus.qrs.deserializer.Deserializer;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.QRData;
import com.nexus.qrs.serializer.BasicSerializer;
import com.nexus.qrs.serializer.Serializer;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;

import static org.junit.jupiter.api.Assertions.*;

public class QrCodeIT {

    private static Converter converter;
    private static Serializer serializer;
    private static Deserializer deserializer;
    private final IQRSContext IQRSContext = new IQRSContext() {};

    @BeforeAll
    public static void init() {
        converter = new SimpleConverter();
        serializer = new BasicSerializer();
        deserializer = new BasicDeserializer();
    }

    @Test
    void testSimpleTextQrCodeIT() throws IOException, WriterException, NotFoundException, ByteStreamConverterNotFound, FormatException {
        File file = TestUtils.loadTestFile("src/test/resources/test-data/input/text/helloWorld.txt");
        assertNotNull(file);

        FileDataChain dataChain = converter.readFile(file, 2000);
        List<String> data = dataChain.chainIterator().next();
        QRData qrCode = serializer.createQrCode(data, 200, 200);
        assertNotNull(qrCode);
        assertNotNull(qrCode.getQrImage());
        assertEquals(200, qrCode.getQrImage().getWidth());
        assertEquals(200, qrCode.getQrImage().getHeight());
        TestUtils.writeQrCodeToFile(qrCode.getQrImage(), "PNG", "src/test/resources/test-data/output/qr/hello-world-INT-QR.png");

        String fileData = deserializer.readQrCode(qrCode.getQrImage());
        assertNotNull(fileData);

        Matcher matcher = IQRSContext.getExtensionPatterns().matcher(file.getName());
        String extension = matcher.find() ? matcher.group(1) :  "";
        List<String> partition = IQRSContext.partition(IQRSContext.stripSequenceCounter(fileData), extension);

        File qrFile = converter.writeFile(partition, "src/test/resources/test-data/output/text/helloWorld-out.txt");
        assertNotNull(qrFile);
        assertTrue(Files.exists(qrFile.toPath()));
    }

    @Test
    void testLargeTextQrCodeIT() throws IOException, WriterException, NotFoundException, ByteStreamConverterNotFound, FormatException {
        File file = TestUtils.loadTestFile("src/test/resources/test-data/input/text/qrCode.txt");
        assertNotNull(file);

        FileDataChain dataChain = converter.readFile(file, 2000);
        List<String> data = dataChain.chainIterator().next();
        QRData qrCode = serializer.createQrCode(data, 200, 200);
        assertNotNull(qrCode);
        assertNotNull(qrCode.getQrImage());
        assertEquals(200, qrCode.getQrImage().getWidth());
        assertEquals(200, qrCode.getQrImage().getHeight());
        TestUtils.writeQrCodeToFile(qrCode.getQrImage(), "PNG", "src/test/resources/test-data/output/qr/qrCode-INT-QR.png");

        String fileData = deserializer.readQrCode(qrCode.getQrImage());
        assertNotNull(fileData);

        Matcher matcher = IQRSContext.getExtensionPatterns().matcher(file.getName());
        String extension = matcher.find() ? matcher.group(1) :  "";
        List<String> partition = IQRSContext.partition(IQRSContext.stripSequenceCounter(fileData), extension);
        File qrFile = converter.writeFile(partition, "src/test/resources/test-data/output/text/qrCode-out.txt");
        assertNotNull(qrFile);
        assertTrue(Files.exists(qrFile.toPath()));
    }

    @Test
    void testMultipleQrCodesIT() throws IOException, WriterException, ByteStreamConverterNotFound {
        int[] codeDims = {185, 185};
        File file = TestUtils.loadTestFile("src/test/resources/test-data/input/text/qrCode-usage.txt");
        assertNotNull(file);

        // Convert the file to a DataChain and get the iterator
        FileDataChain dataChain = converter.readFile(file, 2000);
        assertNotNull(dataChain);
        assertTrue(dataChain.blockCount() > 0);
        Iterator<List<String>> chainIterator = dataChain.chainIterator();

        // Create the QR codes from the DataChain
        AtomicInteger count = new AtomicInteger();
        List<QRData> qrCodes = new ArrayList<>();
        while (chainIterator.hasNext()) {
            List<String> element = chainIterator.next();
            QRData qrCode = serializer.createQrCode(element, codeDims[0], codeDims[1]);
            assertNotNull(qrCode);
            assertNotNull(qrCode.getQrImage());
            assertEquals(codeDims[0], qrCode.getQrImage().getWidth());
            assertEquals(codeDims[1], qrCode.getQrImage().getHeight());
            qrCodes.add(qrCode);

            Path path = Paths.get(String.format(
                    "src/test/resources/test-data/output/qr/multiple/qrCodeUsage/qrCode-usage-QR-%d.png",
                    count.getAndIncrement()));
            assertNotNull(converter.writeImageToFile(qrCode.getQrImage(), "PNG", path.toString()));
        }

        count.set(0);
        for (QRData qr : qrCodes) {
            String fileData = null;
            try {
                fileData = deserializer.readQrCode(qr.getQrImage());
            } catch (NotFoundException | IOException | FormatException e) {
                e.printStackTrace();
            }
            assertNotNull(fileData);

            Matcher matcher = IQRSContext.getExtensionPatterns().matcher(file.getName());
            String extension = matcher.find() ? matcher.group(1) :  "";
            List<String> partition = IQRSContext.partition(IQRSContext.stripSequenceCounter(fileData), extension);
            Path path = Paths.get(String.format("src/test/resources/test-data/output/text/multiple/octal-INT-QR-%d-out.txt", count.getAndIncrement()));
            assertNotNull(converter.writeFile(partition, path.toString()));
            assertTrue(Files.exists(path));
        }
    }

    @Test
    void testImageQrCodesIT() throws IOException, WriterException, ByteStreamConverterNotFound {
        int[] codeDims = {185, 185};
        File file = TestUtils.loadTestFile("src/test/resources/test-data/input/binary/testImages/linux-mascot.png");
        assertNotNull(file);

        // Convert the file to a DataChain and get the iterator
        FileDataChain dataChain = converter.readFile(file, 2000);
        assertNotNull(dataChain);
        assertTrue(dataChain.blockCount() > 0);
        Iterator<List<String>> chainIterator = dataChain.chainIterator();

        // Create the QR codes from the DataChain
        AtomicInteger count = new AtomicInteger();
        List<QRData> qrCodes = new ArrayList<>();
        while (chainIterator.hasNext()) {
            List<String> element = chainIterator.next();
            QRData qrCode = serializer.createQrCode(element, codeDims[0], codeDims[1]);
            assertNotNull(qrCode);
            assertNotNull(qrCode.getQrImage());
            assertEquals(codeDims[0], qrCode.getQrImage().getWidth());
            assertEquals(codeDims[1], qrCode.getQrImage().getHeight());
            qrCodes.add(qrCode);

            Path path = Paths.get(String.format(
                    "src/test/resources/test-data/output/qr/multiple/linuxMascot/linux-mascot-QR-%d.png",
                    count.getAndIncrement()));
            converter.writeImageToFile(qrCode.getQrImage(), "PNG", path.toString());
        }

        // Decode the QR codes and write the data to the designated file
        List<List<String>> partitionData = new ArrayList<>();
        count.set(0);
        for (QRData qr : qrCodes) {
            String fileData = null;
            try {
                fileData = deserializer.readQrCode(qr.getQrImage());
            } catch (NotFoundException | IOException e) {
                e.printStackTrace();
            } catch (FormatException e) {
                throw new RuntimeException(e);
            }
            assertNotNull(fileData);

            Matcher matcher = IQRSContext.getExtensionPatterns().matcher(file.getName());
            String extension = matcher.find() ? matcher.group(1) :  "";

            // Partition data by extension
            List<String> partition = IQRSContext.partition(IQRSContext.stripSequenceCounter(fileData), extension);
            partitionData.add(partition);
        }
        assertNotNull(partitionData);
        assertEquals(141, partitionData.size());
        converter.writeQrCodesToFile(partitionData,
                "src/test/resources/test-data/output/binary/testImages/linux-mascot-out.png");
    }
}
