package com.nexus.qrs.deserializer;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.util.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DeserializerTest {

    private static Deserializer deserializer;

    private final IQRSContext IQRSContext = new IQRSContext() {};

    @BeforeAll
    public static void init() {
        deserializer = new BasicDeserializer();
    }

    @Test
    public void decodeSimpleTextQrCode() throws IOException, NotFoundException, FormatException {
        String[] expectedHex = "48 65 6C 6C 6F 2C 20 77 6F 72 6C 64 21".split(" ");
        String[] expectedBinary = "72 101 108 108 111 44 32 119 111 114 108 100 33".split(" ");

        File qrFile = TestUtils.loadTestFile("src/test/resources/test-data/input/qrCode/hello-world-INT-QR.png");
        String qrCodeData = deserializer.readQrCode(qrFile);
        assertNotNull(qrCodeData);

        //NOTE: This QR code is known to have text
        List<String> partition = IQRSContext.partition(qrCodeData, "txt");

        AtomicInteger counter = new AtomicInteger();
        partition.forEach(octal -> assertEquals(expectedHex[counter.getAndIncrement()], octal,
                "Value is not octal!"));

        counter.set(0);
        partition.forEach(octal ->
            assertEquals(expectedBinary[counter.getAndIncrement()], IQRSContext.hexToDec(octal),
                    "Value is not binary!"));
    }
}
