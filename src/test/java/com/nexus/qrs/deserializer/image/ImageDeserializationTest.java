package com.nexus.qrs.deserializer.image;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.deserializer.BasicDeserializer;
import com.nexus.qrs.deserializer.Deserializer;
import com.nexus.qrs.util.QrSequenceSorter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ImageDeserializationTest {
    private static Deserializer deserializer;

    private final IQRSContext IQRSContext = new IQRSContext() {};

    @BeforeAll
    public static void init() {
        deserializer = new BasicDeserializer();
    }

    @Test
    public void decodeSimpleImageQrCodeBundle() throws IOException {
        List<File> qrFiles = getFiles(Paths.get("src/test/resources/test-data/output/qr/multiple/linuxMascot"));
        sortQrFiles(qrFiles);

        List<List<String>> fileData = new ArrayList<>();
        qrFiles.forEach(qrFile -> {
            try {
                String qrCodeData = deserializer.readQrCode(qrFile);

                Matcher matcher = IQRSContext.getExtensionPatterns().matcher(qrFile.getName());
                String extension = matcher.find() ? matcher.group(1) :  "";
                List<String> partition = IQRSContext.partition(IQRSContext.stripSequenceCounter(qrCodeData), extension);
                assertNotNull(qrCodeData);
                //TODO: assert that each qrCodeData block matches the octal and binary
                fileData.add(partition);
            } catch (IOException | NotFoundException e) {
                e.printStackTrace();
            } catch (FormatException e) {
                throw new RuntimeException(e);
            }
        });
        assertNotNull(fileData);
        assertEquals(141, fileData.size());
    }

    private void sortQrFiles(List<File> files) {
        Map<String, File> fileMap = files.stream()
                .collect(Collectors.toMap(File::getName, Function.identity()));
        files.clear();
        List<String> sortedKeys = fileMap.keySet().stream()
                .sorted(new QrSequenceSorter())
                .collect(Collectors.toList());
        sortedKeys.forEach(key -> files.add(fileMap.get(key)));
    }


    private List<File> getFiles(Path filePath) throws IOException {
        try (Stream<Path> stream = Files.walk(filePath, 1)) {
            return stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        }
    }
}
