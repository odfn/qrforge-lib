package com.nexus.qrs.crypto;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.QRData;
import com.nexus.qrs.util.FileExtensions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import workflows.crypto.CryptoBase;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class CryptoTest extends CryptoBase {

    private static CryptoProvider crypto;

    public CryptoTest() throws NoSuchAlgorithmException, NoSuchProviderException {
    }

    @BeforeAll
    public static void init() {
        crypto = CryptoFactory.getProvider("AES/CBC/PKCS5Padding");
    }

    @Test
    public void symmetricKey_generation() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyGenerator kGen = KeyGenerator.getInstance("AES", "BC");
        int[] keyLength = {128, 192, 256};

        // Generate a key length of 128 bits
        kGen.init(keyLength[0], random);
        SecretKey key128 = kGen.generateKey();
        assertNotNull(key128);

        // Generate a key length of 192 bits
        kGen.init(keyLength[1], random);
        SecretKey key192 = kGen.generateKey();
        assertNotNull(key192);

        // Generate a key length of 256 bits
        kGen.init(keyLength[2], random);
        SecretKey key256 = kGen.generateKey();
        assertNotNull(key256);
    }

    @Test
    public void simpleEncryption() throws NoSuchAlgorithmException, NoSuchProviderException {
        byte[] input = IQRSContext.asciiToHex("This is a test plaintext").getBytes();

        KeyGenerator kGen = KeyGenerator.getInstance("AES", "BC");
        kGen.init(192);
        SecretKey key = kGen.generateKey();

        crypto.setSecretKey(Hex.toHexString(key.getEncoded()));
        byte[] encrypted = crypto.encrypt(input);
        byte[] decrypted = crypto.decrypt(encrypted);
        assertEquals(Hex.toHexString(input), Hex.toHexString(decrypted));
    }

    @Test
    @Disabled
    public void dataChainEncryption() throws NoSuchAlgorithmException, NoSuchProviderException, IOException, WriterException, ByteStreamConverterNotFound {
        Path fp = getInputFile("spaceImage");
        String inputRaw = IQRSContext.getHexBytes(fp);
        int[] size = {400, 400};

        KeyGenerator kGen = KeyGenerator.getInstance("AES", "BC");
        kGen.init(192);
        SecretKey key = kGen.generateKey();

        crypto.setSecretKey(Hex.toHexString(key.getEncoded()));
        byte[] encrypted = crypto.encrypt(Hex.decode(inputRaw));
        assertNotNull(encrypted);

        Instant dcStart, dcEnd;

        dcStart = Instant.now();
        FileDataChain dataChain = converter.readEncryptedString(Hex.toHexString(encrypted),
                fp.toFile().getName(), 2000);
        dcEnd = Instant.now();
        System.out.println("converter.readEncryptedString() duration: " + Duration.between(dcStart, dcEnd));
        assertNotNull(dataChain);

        // Generate the QR Codes
        dcStart = Instant.now();
        List<QRData> qrCodes = serializer.createQrCodes(dataChain.getContents(),size[0], size[1]);
        dcEnd = Instant.now();
        System.out.println("serializer.createQrCodes() duration: " + Duration.between(dcStart, dcEnd));

        // Collect the payloads
        dcStart = Instant.now();
        List<String> payloads = qrCodes.stream()
                .map(qrData -> {
                    try {
                        return deserializer.readQrCode(qrData.getQrImage());
                    } catch (IOException | NotFoundException | FormatException e) {
                        throw new RuntimeException(e);
                    }
                })
                .toList();
        dcEnd = Instant.now();
        System.out.println("payloads duration: " + Duration.between(dcStart, dcEnd));

        // At this point, the payload is still partitioned QR data.
        // Rebuild the payload with a method 'reconstructFile()'
        List<List<String>> partitions = payloads.stream()
                .map(dataIn -> IQRSContext.partition(IQRSContext.stripSequenceCounter(dataIn)))
                .collect(Collectors.toList());
        byte[] fileData = IQRSContext.reconstructFile(partitions, "txt");

        byte[] decrypted = crypto.decrypt(fileData);
        assertEquals(Hex.toHexString(Hex.decode(inputRaw)), Hex.toHexString(decrypted));

        // Write decrypted result to filesystem
        try (FileOutputStream fOut = new FileOutputStream(outputSourcePath.resolve("binary/testImages/result.jpg").toString());
             Writer writer = new OutputStreamWriter(fOut)) {

            String type = FileExtensions.getInstance().queryExtension(fp.getFileName().toString().split("\\.")[1]);
            switch (type) {
                case "TEXT" -> {
                    for (byte b : decrypted)
                        writer.write(b);
                }
                case "IMAGE", "AUDIO" -> {
                    for (byte b : decrypted)
                        fOut.write(b);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
