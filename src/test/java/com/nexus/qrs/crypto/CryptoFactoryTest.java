package com.nexus.qrs.crypto;

import com.nexus.qrs.IQRSContext;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import workflows.crypto.CryptoBase;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CryptoFactoryTest extends CryptoBase {

    protected final IQRSContext IQRSContext = new IQRSContext() {};

    public CryptoFactoryTest() throws NoSuchAlgorithmException, NoSuchProviderException {
    }

    @Test
    @Disabled
    public void cryptoFactoryTest() {
        CryptoProvider crypto = CryptoFactory.getProvider("AES/CBC/PKCS5Padding");
        assertNotNull(crypto);

        crypto.setSecretKey("000102030405060708090a0b0c0d0e0f");
        byte[] input = IQRSContext.asciiToHex("This is a test").getBytes();

        byte[] encrypted = crypto.encrypt(input);
        assertNotNull(encrypted);

        byte[] decrypted = crypto.decrypt(encrypted);
        assertNotNull(decrypted);

        assertEquals(Hex.toHexString(input), Hex.toHexString(decrypted));
    }
}
