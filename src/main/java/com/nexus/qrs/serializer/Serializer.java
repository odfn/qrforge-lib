package com.nexus.qrs.serializer;

import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.model.BoofQRData;
import com.nexus.qrs.model.QRData;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface Serializer extends IQRSContext {
    QRData createQrCode(List<String> data, int width, int height) throws WriterException, IOException;

    QRData createQrCode(List<String> data, int width, int height, HashMap<EncodeHintType, ?> hints) throws WriterException, IOException;

    List<QRData> createQrCodes(List<List<String>> chainData, int width, int height);

    default BoofQRData createBoofQrCodeM1(List<String> data, int width, int height) {
        return null;
    }

    default BoofQRData createBoofQrCodeM2(List<String> data, int width, int height, HashMap<EncodeHintType, ?> hints) throws WriterException, IOException {
        return null;
    }

    default List<BoofQRData> createBoofQrCodesM3(List<List<String>> chainData, int width, int height) {
        return null;
    }
}
