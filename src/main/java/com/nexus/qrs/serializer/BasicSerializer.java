package com.nexus.qrs.serializer;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.nexus.qrs.model.QRData;
import com.nexus.qrs.model.hash.QrHash;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class BasicSerializer implements Serializer {
    private final QRCodeWriter writer;
    private QrHash hash;

    private final HashMap<EncodeHintType, Object> hints;

    public BasicSerializer() {
        this.writer = new QRCodeWriter();
        this.hints = new HashMap<>();
        this.hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    }

    @Override
    public QRData createQrCode(List<String> data, int width, int height) throws WriterException {
        return createQrCode(data, width, height, hints);
    }

    @Override
    public QRData createQrCode(List<String> data, int width, int height, HashMap<EncodeHintType, ?> hints) {
        String collectedData = String.join("", data);
        BufferedImage image;
        try {
            BitMatrix bitMatrix = writer.encode(collectedData, BarcodeFormat.QR_CODE, width, height, hints);
            image = MatrixToImageWriter.toBufferedImage(bitMatrix);
        } catch (WriterException e) {
            throw new RuntimeException(e);
        }
        //TODO: Find an efficient hash algo to generate QrHashIDs
        return new QRData(image, collectedData, null);
    }

    @Override
    public List<QRData> createQrCodes(List<List<String>> chainData, int width, int height) {

        return chainData.stream()
                .map(link -> createQrCode(link, width, height, hints))
                .toList();
    }
}
