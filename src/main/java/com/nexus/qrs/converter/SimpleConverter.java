package com.nexus.qrs.converter;

import com.nexus.qrs.converter.bytestream.ByteStreamConverter;
import com.nexus.qrs.converter.bytestream.ByteStreamConverterFactory;

import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.HeaderDataChain;
import com.nexus.qrs.util.FileExtensions;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class SimpleConverter implements Converter {

    private static final Charset UTF8 = StandardCharsets.UTF_8;
    private final Pattern extensionPattern = Pattern.compile("(\\w+$)");
    private static final FileExtensions fileExtensions = FileExtensions.getInstance();

    public SimpleConverter() throws IllegalArgumentException {
    }

    @Override
    public HeaderDataChain readHeaderString(String inputString, String name, int limiter) {
        return readHeaderString(inputString, name, BlockCapacity.HEX, limiter);
    }

    @Override
    public HeaderDataChain readHeaderString(String inputString, String name, BlockCapacity blockCapacity, int limiter){
        if (inputString == null || inputString.isEmpty() ||
                name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Empty or blank string content found.");
        }

        ByteStreamConverter converter = ByteStreamConverterFactory.getDefaultConverter();
        HeaderDataChain dataChain = new HeaderDataChain(name, blockCapacity);

        Arrays.stream(inputString.split(""))
                .forEach(str -> dataChain.addData(converter.asciiToHex(str)));

        dataChain.setFileSize(inputString.length());
        return dataChain;
    }

    @Override
    public FileDataChain readEncryptedString(String inputString, String name, int limiter) {
        return readEncryptedString(inputString, name, BlockCapacity.HEX, limiter);
    }

    @Override
    public FileDataChain readEncryptedString(String inputString, String name, BlockCapacity blockCapacity, int limiter) {
        if (inputString == null || inputString.isEmpty() ||
                name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Empty or blank string content found.");
        }

        FileDataChain dataChain = new FileDataChain(name, blockCapacity, 2000);
        writeToDataChain(new ByteArrayInputStream(inputString.getBytes()), dataChain);
        dataChain.setFileSize(inputString.length());
        return dataChain;
    }

    @Override
    public FileDataChain readFile(String filePath, int limiter) throws IOException {
        return readFile(Paths.get(filePath).toFile(), BlockCapacity.HEX, limiter);
    }

    @Override
    public FileDataChain readFile(File file, BlockCapacity blockCapacity, int limiter) throws IOException {
        Path fp = file.toPath();
        if (Files.notExists(fp)) {
            throw new IllegalArgumentException("File not found! " + fp);
        }

        FileDataChain dataChain = new FileDataChain(file.getName(), blockCapacity, limiter);

        writeToDataChain(Files.newInputStream(fp), dataChain);
        dataChain.setFileSize(Files.size(file.toPath()));
        return dataChain;
    }

    @Override
    public FileDataChain readFile(File file, int limiter) throws IOException {
        return readFile(file, BlockCapacity.HEX, limiter);
    }

    @Override
    public FileDataChain readFile(MultipartFile file, BlockCapacity blockCapacity, int limiter) throws IOException {
        FileDataChain dataChain = new FileDataChain(file.getName(), blockCapacity, limiter);

        writeToDataChain(file.getInputStream(), dataChain);
        dataChain.setFileSize(file.getSize());
        return dataChain;
    }

    private File convertMultipartFileToFile(MultipartFile mpFile) throws IOException {
        File inputFile = Files.createTempFile(mpFile.getOriginalFilename(), null).toFile();
        mpFile.transferTo(inputFile);
        return inputFile;
    }

    private void writeToDataChain(InputStream inStream, FileDataChain dataChain){
        ByteStreamConverter converter = ByteStreamConverterFactory.getDefaultConverter();
        dataChain.addData(inStream, converter);
    }

    @Override
    public FileDataChain readFile(MultipartFile file, int limiter) throws  IOException {
        return readFile(file, BlockCapacity.HEX, limiter);
    }

    @Override
    public FileDataChain readFile(InputStream inputStream, String fileName, BlockCapacity blockCapacity, int limiter) throws IOException {
        FileDataChain dataChain = new FileDataChain(fileName, blockCapacity, limiter);

        writeToDataChain(inputStream, dataChain);
        return dataChain;
    }

    @Override
    public FileDataChain readFile(InputStream inputStream, String fileName, int limiter) throws IOException {
        return readFile(inputStream, fileName, BlockCapacity.HEX, limiter);
    }

    @Override
    public File writeFile(List<String> fileData, String filePath) {
        Path outputPath = Paths.get(filePath);
        String fileExtension = parseFileExtension(outputPath);

        try {
            byte[] reconstructedFile = reconstructFile(Collections.singletonList(fileData), fileExtension);
            writeFileBytes(reconstructedFile, filePath, fileExtension, true);
        } catch (IOException e) {
            throw new RuntimeException("Error writing file: " + filePath, e);
        }

        return outputPath.toFile();
    }

    @Override
    public File writeQrCodesToFile(List<List<String>> fileData, String filePath) {
        Path outputPath = Paths.get(filePath);
        String fileExtension = parseFileExtension(outputPath);

        try {
            byte[] reconstructedFile = reconstructFile(fileData, fileExtension);
            String fileType = fileExtensions.queryExtension(fileExtension);
            writeFileBytes(reconstructedFile, filePath, fileType, "TEXT".equals(fileType));
        } catch (IOException e) {
            throw new RuntimeException("Error writing QR codes to file: " + filePath, e);
        }

        return outputPath.toFile();
    }

    private void writeFileBytes(byte[] data, String filePath, String fileType, boolean asText) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(filePath);
             Writer writer = asText ? new OutputStreamWriter(outputStream, UTF8) : null) {
            for (byte b : data) {
                if (asText) {
                    writer.write(b);
                } else {
                    outputStream.write(b);
                }
            }
        }
    }

    private String parseFileExtension(Path filePath) {
        String[] parts = filePath.getFileName().toString().split("\\.");
        return parts.length > 1 ? parts[parts.length - 1] : "";
    }
}
