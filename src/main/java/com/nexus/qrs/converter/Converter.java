package com.nexus.qrs.converter;

import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.HeaderDataChain;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public interface Converter extends IQRSContext {

    HeaderDataChain readHeaderString(String inputString, String name, int limiter);

    HeaderDataChain readHeaderString(String inputString, String name, BlockCapacity blockCapacity, int limiter);

    FileDataChain readEncryptedString(String inputString, String name, int limiter);

    FileDataChain readEncryptedString(String inputString, String name, BlockCapacity blockCapacity, int limiter);

    /**
     * Reads a file and creates a DataChain
     * @param filePath The path of the file to be converted to a DataChin
     * @return A DataChain with octal data encoding
     */
    FileDataChain readFile(String filePath, int limiter) throws IOException, ByteStreamConverterNotFound;

    /**
     * Reads a file and creates a DataChain with specified blockCapacity
     * @param file The file to be converted to a DataChin
     * @param blockCapacity The block capacity of the DataChain
     * @return A DataChain with octal data encoding
     */
    FileDataChain readFile(File file, BlockCapacity blockCapacity, int limiter) throws ByteStreamConverterNotFound, IOException;

    /**
     * Reads a file and creates a DataChain
     * @param file The file to be converted to a DataChin
     * @return A DataChain with octal data encoding
     */
    FileDataChain readFile(File file, int limiter) throws ByteStreamConverterNotFound, IOException;

    /**
     * Receives a file from a network upload and creates a DataChain with specified blockCapacity
     * @param file The network upload file to be converted to a DataChin
     * @param blockCapacity The block capacity of the DataChain
     * @return A DataChain with octal data encoding
     */
    FileDataChain readFile(MultipartFile file, BlockCapacity blockCapacity, int limiter) throws ByteStreamConverterNotFound, IOException;

    /**
     * Receives a file from a network upload and creates a DataChain
     * @param file The network upload file to be converted to a DataChin
     * @return A DataChain with octal data encoding
     */
    FileDataChain readFile(MultipartFile file, int limiter) throws ByteStreamConverterNotFound, IOException;

    /*******************************************************************/

    /**
     * Receives a file from an InputStream upload and creates a DataChain with specified blockCapacity
     * @param inputStream The InputStream upload file to be converted to a DataChin
     * @param blockCapacity The block capacity of the DataChain
     * @return A DataChain with octal data encoding
     */
    FileDataChain readFile(InputStream inputStream, String fileName, BlockCapacity blockCapacity, int limiter) throws ByteStreamConverterNotFound, IOException;

    /**
     * Receives a file from an InputStream upload and creates a DataChain
     * @param inputStream The InputStream upload file to be converted to a DataChin
     * @param fileName The file name for the DataChain
     * @param limiter Capacity limiter for DataChain links
     * @return A DataChain with octal data encoding
     */
    FileDataChain readFile(InputStream inputStream, String fileName, int limiter) throws ByteStreamConverterNotFound, IOException;

    /**
     * Writes fileData of a QR code to a file
     * @param fileData A partitioned stream of file data
     * @param filePath The location of the file to be saved
     * @return The decoded file
     */
    File writeFile(List<String> fileData, String filePath) throws ByteStreamConverterNotFound;

    /**
     * Writes a collection of fileData of a QR bundle to a file
     * @param fileData A collection of fileData
     * @param filePath The location of the file to be saved
     * @return The decoded file
     */
    File writeQrCodesToFile(List<List<String>> fileData, String filePath) throws ByteStreamConverterNotFound;


    /**
     * Writes a QR image to a file
     * @param qrCode The QR Code image
     * @param fileFormat File format of the image
     * @param qrPath The location of the file to be saved
     * @throws IOException
     */
    default File writeImageToFile(BufferedImage qrCode, String fileFormat, String qrPath) throws IOException {
        File file = Paths.get(qrPath).toFile();
        return ImageIO.write(qrCode, fileFormat, file) ? file: null;
    }
}
