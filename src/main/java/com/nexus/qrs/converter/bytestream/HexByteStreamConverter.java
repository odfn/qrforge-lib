package com.nexus.qrs.converter.bytestream;

public class HexByteStreamConverter implements ByteStreamConverter{
    @Override
    public String convert(int inValue) {
        return IQRS_CONTEXT.toHex(inValue);
    }
}
