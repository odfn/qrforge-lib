package com.nexus.qrs.converter.bytestream;

import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.exceptions.converter.ByteStreamConverterNotFound;
import com.nexus.qrs.util.FileExtensions;

public class ByteStreamConverterFactory {

    private static IQRSContext IQRSContext = new IQRSContext() {};

    private ByteStreamConverterFactory() {}

    /**
     * Returns the ByteStreamConverter (Octal, Hex) based on filetype.
     * Deprecated for now.....
     * @param extension The file extension of the file being converted
     * @return The associated ByteStreamConverter by type
     * @throws ByteStreamConverterNotFound
     */
    @Deprecated
    public static ByteStreamConverter getConverter(String extension) throws ByteStreamConverterNotFound {
        ByteStreamConverter converter;

        switch (FileExtensions.getInstance().queryExtension(extension)) {
            case "TEXT":
            case "IMAGE":
            case "AUDIO":
            case "VIDEO":
            case "BINARY":
                converter = new HexByteStreamConverter();
                break;
            case "STRING":
                converter = new StringByteStreamConverter();
            default:
                throw new ByteStreamConverterNotFound(extension);
        }
        return converter;
    }

    /**
     * Returns the default converter
     * @return The HexByteStreamConverter
     */
    public static ByteStreamConverter getDefaultConverter() {
        return new HexByteStreamConverter();
    }
}