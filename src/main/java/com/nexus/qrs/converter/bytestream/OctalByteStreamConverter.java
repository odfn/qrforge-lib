package com.nexus.qrs.converter.bytestream;

public class OctalByteStreamConverter implements ByteStreamConverter{

    @Override
    public String convert(int inValue) {
        return IQRS_CONTEXT.toOctal(inValue);
    }
}
