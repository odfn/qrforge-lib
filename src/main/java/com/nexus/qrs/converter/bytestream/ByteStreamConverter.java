package com.nexus.qrs.converter.bytestream;

import com.nexus.qrs.IQRSContext;

/**
 * The BytesStreamConverter converts the bytes read in from a file to a
 * different encoding
 */
public interface ByteStreamConverter {
    IQRSContext IQRS_CONTEXT = new IQRSContext() {};

    public String convert(int inValue);

    default String asciiToHex(String strValue) {
        return IQRS_CONTEXT.asciiToHex(strValue);
    }
}
