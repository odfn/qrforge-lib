package com.nexus.qrs.converter.bytestream;

import com.nexus.qrs.IQRSContext;

public class PassThroughByteStreamConverter implements ByteStreamConverter{
    private IQRSContext IQRSContext = new IQRSContext() {};

    @Override
    public String convert(int inValue) {
        return IQRSContext.passThrough(inValue);
    }
}
