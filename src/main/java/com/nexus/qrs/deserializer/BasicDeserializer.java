package com.nexus.qrs.deserializer;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.qrcode.QRCodeMultiReader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class BasicDeserializer implements Deserializer {

    private final Reader reader;

    public BasicDeserializer() {
        reader = new QRCodeMultiReader();
    }

    @Override
    public String readQrCode(String filePath) throws IOException, NotFoundException, FormatException {
        return readQrCode(Paths.get(filePath).toFile());
    }

    /**
     * Takes a QR code image, decodes it and returns the payload
     * @param image The QR code image
     * @return The payload data of the QR code
     */
    @Override
    public String readQrCode(BufferedImage image) throws NotFoundException, FormatException {
        LuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(source));
        Map<DecodeHintType, Object> hints = new HashMap<>();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

        Result result = null;
        try {
            result = reader.decode(binaryBitmap, hints);
        } catch (ChecksumException e) {
            throw new RuntimeException(e);
        }
        return result != null ? result.toString() : null;
    }

    /**
     * Takes a QR code image, decodes it and returns the payload
     * @param qrFile The QR code file
     * @return The payload data of the QR code
     */
    @Override
    public String readQrCode(File qrFile) throws IOException, NotFoundException, FormatException {
        BufferedImage image = ImageIO.read(qrFile);
        return readQrCode(image);
    }

}
