package com.nexus.qrs.deserializer;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.nexus.qrs.IQRSContext;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public interface Deserializer extends IQRSContext {

    String readQrCode(String filePath) throws IOException, NotFoundException, FormatException;

    /**
     * Takes a QR code file, decodes it and returns the payload
     * @param qrFile The QR code file
     * @return The payload data of the QR code
     */
    String readQrCode(File qrFile) throws IOException, NotFoundException, FormatException;

    /**
     * Takes a QR code image, decodes it and returns the payload
     * @param image The QR code image
     * @return The payload data of the QR code
     */
    String readQrCode(BufferedImage image) throws IOException, NotFoundException, FormatException;

}
