package com.nexus.qrs.crypto;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

public class CryptoFactory {

    static {
        // Setup Unlimited Strength Jurisdiction Policy Files
        Security.setProperty("crypto.policy", "unlimited");
        // Setup Encryption provider
        Security.addProvider(new BouncyCastleProvider());
    }

    public static CryptoProvider getProvider(String transformation) {
        String[] transformParams = transformation.split("/");
        String cipher = transformParams[0];
        String blockMode = transformParams[1];
        String padding = transformParams[2];

        if ("ECB".equalsIgnoreCase(blockMode))
            throw new IllegalArgumentException("Use of ECB Mode not supported");

        Cipher cipherInstance;
        try {
            cipherInstance = Cipher.getInstance(transformation, "BC");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
        return new CryptoProvider(cipherInstance);
    }
}
