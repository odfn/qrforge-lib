package com.nexus.qrs.exceptions.converter;

public class ByteStreamConverterNotFound extends Exception {

    public ByteStreamConverterNotFound(String extension) {
        super("No ByteStreamConverter was found for extension: " + extension);
    }
}
