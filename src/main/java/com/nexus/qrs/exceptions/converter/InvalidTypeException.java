package com.nexus.qrs.exceptions.converter;

public class InvalidTypeException extends RuntimeException{

    public InvalidTypeException(String type) {
        super("Invalid type: " + type);
    }
}
