package com.nexus.qrs;

import com.nexus.qrs.converter.bytestream.ByteStreamConverter;
import com.nexus.qrs.converter.bytestream.ByteStreamConverterFactory;
import com.nexus.qrs.exceptions.converter.InvalidTypeException;
import com.nexus.qrs.util.FileExtensions;
import com.nexus.qrs.util.QrSequenceSorter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface IQRSContext {
    Pattern extensionPattern = Pattern.compile("(\\w+$)");

    default Pattern getExtensionPatterns() {
        return extensionPattern;
    }

    /**
     * Retrieve all the files from the given filePath
     * @param filePath A path
     * @return the files in the path
     * @throws IOException
     */
    default List<File> getFiles(Path filePath) throws IOException {
        try (Stream<Path> stream = Files.walk(filePath, 1)) {
            return stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        }
    }

    default void sortQrFiles(List<File> files) {
        //TODO: Create a SortingAlgorithmFactory to retrieve sorting implementations
        // to handle sorting arbitrarily large datasets
        sorter.accept(files);
    }

    Consumer<List<File>> sorter = files -> {
        Map<String, File> fileMap = files.stream()
                .collect(Collectors.toMap(File::getName, Function.identity()));
        files.clear();
        List<String> sortedKeys = fileMap.keySet().stream()
                .sorted(new QrSequenceSorter())
                .toList();
        sortedKeys.forEach(key -> files.add(fileMap.get(key)));
    };

    default String stripSequenceCounter(String dataIn) {
        return dataIn.substring(0, dataIn.indexOf("|"));
    }

    default String getSequenceCounter(String dataIn) {
        return dataIn.substring(dataIn.indexOf("|"));
    }

    /**
     * Converts the data from the QR code into a partitioned stream
     * (Future use case(s): Writing file QR Codes to filesystem and crypto use cases)
     * @param dataIn The data from the QR code
     * @return A partitioned stream
     */
    default List<String> partition(String dataIn, String extension) {
        String type = FileExtensions.getInstance().queryExtension(extension);
//        dataIn = dataIn.substring(0, dataIn.indexOf(":"));

        String[] split = dataIn.split("");
        Iterator<String> iterator = Arrays.stream(split).iterator();

        List<String> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        int byteCadence = 2;
        while (iterator.hasNext()) {
            for(int i = 0; i < byteCadence; i++)
                sb.append(iterator.next());
            result.add(sb.toString());
            sb.delete(0, sb.length());
        }

        return result;
    }

    default List<String> partition(String dataIn) {
        return partition(dataIn, "");
    }

    enum BlockCapacity {
        HEX(2148);     // 4296 ascii chars / 2 (hex bytes) = 2148 entries

        private final int value;

        BlockCapacity(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    enum ChainType {
        SEQUENTIAL("Sequential"),
        LUBY("Luby"),
        HCQR("HCQR");

        private final String type;

        ChainType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    default String toOctal(int intValue) {
        return String.format("%03o", intValue);
    }

    default String passThrough(int intValue) {
        return String.format("%d", intValue);
    }

    default String toHex(int intValue) {
        return String.format("%02X", intValue);
    }

    default String toHex(String value) {
        return String.format("%02X", Integer.parseInt(value));
    }

    default String asciiToHex(String value) {
        return String.format("%x", new BigInteger(1, value.getBytes(/*YOUR_CHARSET?*/)));
    }

    default String hexToAscii(String value) {
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < value.length(); i += 2) {
            String str = value.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        return output.toString();
    }

    default String hexToDec(String value) {
        return new BigInteger(value, 16).toString(10);
    }

    default String toDec(Character strChar) {
        return String.format("%d", strChar.toString().getBytes()[0]);
    }

    default byte[] reconstructFile(List<List<String>> fileData, String extension) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (List<String> qrcode: fileData) {
            baos.write(getBytes(qrcode, extension));
        }
        return baos.toByteArray();
    }

    default byte[] reconstructFile(List<List<String>> fileData) throws IOException{
        return reconstructFile(fileData, "");
    }

    default byte[] getBytes(List<String> fileData, String extension) throws NullPointerException {
        String type = FileExtensions.getInstance().queryExtension(extension);

        // Convert fileData from hex to decimal
        List<String> binaryData = switch (type) {
            case "TEXT", "IMAGE", "AUDIO", "VIDEO", "BINARY" -> fileData.stream()
                    .map(this::hexToDec)
                    .collect(Collectors.toList());
            case "" -> fileData.stream()
                    .map(this::asciiToHex)
                    .collect(Collectors.toList());
            default -> throw new InvalidTypeException(type);
        };

        // Convert the binaryData elements from Strings to byte data
        byte[] dataBytes = new byte[binaryData.size()];
        AtomicInteger c = new AtomicInteger();
        binaryData.forEach(e -> dataBytes[c.getAndIncrement()] = (byte) Integer.parseInt(e));
        return dataBytes;
    }

    default String getHexBytes(Path fp) {
        int byteData;
        StringBuilder sb = new StringBuilder();
        ByteStreamConverter converter = ByteStreamConverterFactory.getDefaultConverter();
        try (InputStream is = new BufferedInputStream(Files.newInputStream(fp))) {
            while ((byteData = is.read()) != -1) {
                sb.append(converter.convert(byteData));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String output = sb.toString();
        return output;
    }

    default File jpegToPng(File jpeg)  {
        File png = null;
        try {
            BufferedImage bufferedJpeg = ImageIO.read(jpeg);
            Path jpegPath = Paths.get(jpeg.getPath());
            String s = jpeg.getName().split("\\.")[0] + ".png";
            Path pngPath = jpegPath.getParent().resolve(s);
            png = Paths.get(pngPath.toString()).toFile();
            ImageIO.write(bufferedJpeg, "png", png);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return png;
    }

    default boolean hexBaseCheck(String input) {
        int length = input.split("").length;
        return length % 16 == 0;
    }

    default boolean isHex(String value) {
        try {
            Long.parseLong(value, 16);
            return true;
        } catch (NumberFormatException ignored) {
            return false;
        }
    }

    default boolean isDec(String value) {
        try {
            Long.parseLong(value, 10);
            return true;
        } catch (NumberFormatException ignored) {
            return false;
        }
    }

    BiFunction<List<Integer>, List<Integer>, List<Integer>> arrayListXOR = (List<Integer> left, List<Integer> right) -> {
        List<Integer> xorBits = new ArrayList<>();
        for (int x = 0; x < left.size(); x++) {
            xorBits.add((left.get(x) ^ right.get(x)));
        }
        return xorBits;
    };

    default List<Integer> hex2Decimal(List<String> hex) {
        hex.replaceAll(this::hexToDec);

        return hex.stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    default List<String> payloadXOR(List<String>... payloads) {
        if (payloads.length == 1)
            return payloads[0];

        // Hex to Decimal conversion of QR payloads
        List<List<Integer>> _payloads = new ArrayList<>();
        for(List<String> payload: payloads) {
            _payloads.add(hex2Decimal(payload));
        }

        Iterator<List<Integer>> payloadIter = _payloads.iterator();
        List<Integer> accumulator = new ArrayList<>();
        while (payloadIter.hasNext()) {
            List<Integer> p1 = payloadIter.next();
            List<Integer> p2 = payloadIter.next();
            accumulator = arrayListXOR.apply(p1, p2);

            while (payloadIter.hasNext()) {
                accumulator = arrayListXOR.apply(accumulator, payloadIter.next());
            }
        }

        return accumulator.stream().map(this::toHex).collect(Collectors.toList());
    }
}
