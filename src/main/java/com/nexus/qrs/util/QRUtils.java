package com.nexus.qrs.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexus.qrs.model.FileDataChain;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;

public class QRUtils {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static Path archiveChainData(FileDataChain fileDataChain, Path archivePath) {
        try {
            String jsonChain = mapper.writeValueAsString(fileDataChain);

            Path tempFile = Files.createTempFile(fileDataChain.getChainName(), ".json");

            SevenZOutputFile sevenZOutput = new SevenZOutputFile(archivePath.toFile());
            SevenZArchiveEntry entry = sevenZOutput.createArchiveEntry(tempFile.toFile(), archivePath.toFile().getName());
            sevenZOutput.putArchiveEntry(entry);

            Instant archiveWriteStart, archiveWriteEnd;
            archiveWriteStart = Instant.now();
            sevenZOutput.write(jsonChain.getBytes());
            archiveWriteEnd = Instant.now();

            System.out.println("Archive write duration: " + Duration.between(archiveWriteStart, archiveWriteEnd));
            sevenZOutput.closeArchiveEntry();
            sevenZOutput.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return archivePath;
    }
}
