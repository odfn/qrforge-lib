package com.nexus.qrs.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileExtensions {
    private static List<String> TEXT_TYPES;
    private static List<String> IMAGE_TYPES;
    private static List<String> AUDIO_TYPES;
    private static List<String> VIDEO_TYPES;
    private static List<String> BINARY_TYPES;
    private static FileExtensions INSTANCE = null;

    private FileExtensions() {
        TEXT_TYPES = new ArrayList<>();
        IMAGE_TYPES = new ArrayList<>();
        AUDIO_TYPES = new ArrayList<>();
        VIDEO_TYPES = new ArrayList<>();
        BINARY_TYPES = new ArrayList<>();
    }

    public static FileExtensions getInstance() {
        if (Objects.isNull(INSTANCE)) {
            INSTANCE = new FileExtensions();
            ObjectMapper mapper = new ObjectMapper();
            try (InputStream systemResourceAsStream = FileExtensions.class.getResourceAsStream("/json/supportedFileTypes.json")) {
                JsonNode fileTypes = mapper.readTree(systemResourceAsStream);
                fileTypes.get("filetypes").get("text").forEach(node -> {
                    TEXT_TYPES.add(node.textValue());
                });
                fileTypes.get("filetypes").get("image").forEach(node -> {
                    IMAGE_TYPES.add(node.textValue());
                });
                fileTypes.get("filetypes").get("audio").forEach(node -> {
                    AUDIO_TYPES.add(node.textValue());
                });
                fileTypes.get("filetypes").get("video").forEach(node -> {
                    VIDEO_TYPES.add(node.textValue());
                });
                fileTypes.get("filetypes").get("binary").forEach(node -> {
                    BINARY_TYPES.add(node.textValue());
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return INSTANCE;
    }

    /**
     * Given an extension, return the file type (text, image, audio)
     * @param extension The file extension
     * @return The file type (text, image, audio)
     */
    public String queryExtension(String extension) {
        extension = extension.toLowerCase();
        if (TEXT_TYPES.contains(extension)) {
            return "TEXT";
        } else if (IMAGE_TYPES.contains(extension)) {
            return "IMAGE";
        } else if (AUDIO_TYPES.contains(extension)) {
            return "AUDIO";
        } else if (VIDEO_TYPES.contains(extension)) {
            return "VIDEO";
        }  else if (BINARY_TYPES.contains(extension)) {
            return "BINARY";
        } else
            return "";
    }
}
