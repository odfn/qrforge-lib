package com.nexus.qrs.util;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QrSequenceSorter implements Comparator<String> {
    private final Pattern qrCodePattern = Pattern.compile("(.*)-QR-(\\d+)(.*)");

    @Override
    public int compare(String o1, String o2) {
        Matcher m1 = qrCodePattern.matcher(o1);
        Matcher m2 = qrCodePattern.matcher(o2);

        int v1 = m1.find()? Integer.parseInt(m1.group(2)): -1;
        int v2 = m2.find()? Integer.parseInt(m2.group(2)): -1;
        return v1-v2;
    }
}
