package com.nexus.qrs.model;

import java.awt.image.BufferedImage;

public class QRData {

    private BufferedImage qrImage;
    private String payload;
    private String qrHash;

    public QRData(BufferedImage qrImage, String payload, String qrHash) {
        this.qrImage = qrImage;
        this.payload = payload;
        this.qrHash = qrHash;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public BufferedImage getQrImage() {
        return qrImage;
    }

    public void setQrImage(BufferedImage qrImage) {
        this.qrImage = qrImage;
    }

    public String getQrHash() {
        return qrHash;
    }

    public void setQrHash(String qrHash) {
        this.qrHash = qrHash;
    }
}
