package com.nexus.qrs.model.datamap;

import java.util.Map;

public interface DataMap<K, V> {
    V createEntry();

    int size();

    Map.Entry<K, V> getFirst();

    Map.Entry<K, V> getLast();

    V getNext(K key);

    V getPrevious(K key);

    void add(V header);

    V get(K key);
}
