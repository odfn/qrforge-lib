package com.nexus.qrs.model.datamap;

import java.util.HashMap;
import java.util.Map;

public class HeaderData {
    public enum HeaderDataType {
        SEQ(0),
        LUBY(1),
        HICAP(2);

        private int type;
        HeaderDataType(int type) {
            this.type = type;
        }
    }

    // The type of header data
    private HeaderDataType type;

    // Hash of the link in the FileDataChain
    private String link;

    // Pointer to the hash of the next link in the FileDataChain
    private String nextLink;

    // Pointer to the has of the previous link in the FileDataChain
    private String prevLink;

    // The composition degree (for LUBY FileDataChains)
    private Integer degree;

    // Index of the link in the FileDataChain
    private Integer index;

    // Metadata properties for the header data
    private Map<String, String> props;

    public HeaderData(HeaderDataType type) {
        this.type = type;
        this.props = new HashMap<>();
    }

    public HeaderDataType getType() {
        return type;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        this.degree = degree;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Map<String, String> getProps() {
        return props;
    }

    public void setProps(Map<String, String> props) {
        this.props = props;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

    public String getPrevLink() {
        return prevLink;
    }

    public void setPrevLink(String prevLink) {
        this.prevLink = prevLink;
    }
}
