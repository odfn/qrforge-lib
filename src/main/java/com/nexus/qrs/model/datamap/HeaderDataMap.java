package com.nexus.qrs.model.datamap;

import com.nexus.qrs.model.FileDataChain;
import com.nexus.qrs.model.hash.QrHash;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class HeaderDataMap implements DataMap<String, HeaderData> {

    private final HeaderData.HeaderDataType type;
    private final Map<String, HeaderData> headerMap;
    private Map.Entry<String, HeaderData> headEntry;
    private Map.Entry<String, HeaderData> tailEntry;
    private HeaderData priorEntry;

    public HeaderDataMap(HeaderData.HeaderDataType type) {
        this.type = type;
        this.headerMap = new HashMap<>();
        this.priorEntry = null;
    }

    public static void setHeaders(FileDataChain dataChain, HeaderDataMap headerDataMap, QrHash qrHash) {
        List<List<String>> chainData = dataChain.getDataChain();
        for (int i = 0, dataChainSize = chainData.size(); i < dataChainSize; i++) {
            try {
                List<String> link = chainData.get(i);
                HeaderData headerEntry = headerDataMap.createEntry();
                headerEntry.setLink(qrHash.getHash(link, (long) i));
                headerEntry.setIndex(i);
                headerDataMap.add(headerEntry);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public HeaderData createEntry() {
        return new HeaderData(type);
    }

    @Override
    public int size() {
        return headerMap.size();
    }

    @Override
    public Map.Entry<String, HeaderData> getFirst() {
        if (headEntry == null) {
            List<Map.Entry<String, HeaderData>> headerFound = headerMap.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().getPrevLink() == null)
                    .toList();
            if (headerFound.isEmpty()) throw new AssertionError();
            headEntry = headerFound.get(0);
        }
        return headEntry;
    }

    @Override
    public Map.Entry<String, HeaderData> getLast() {
        if (tailEntry == null) {
            List<Map.Entry<String, HeaderData>> tailFound = headerMap.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().getNextLink() == null)
                    .toList();
            if (tailFound.isEmpty()) throw new AssertionError();
            tailEntry = tailFound.get(0);
        }
        return tailEntry;
    }

    @Override
    public HeaderData getNext(String key) {
        HeaderData headerData = headerMap.get(key);
        return headerMap.get(headerData.getNextLink());
    }

    @Override
    public HeaderData getPrevious(String key) {
        HeaderData headerData = headerMap.get(key);
        return headerMap.get(headerData.getPrevLink());
    }

    @Override
    public void add(HeaderData header) {
        if (headerMap.isEmpty() && Objects.isNull(priorEntry)) {
            headerMap.put(header.getLink(), header);
            priorEntry = header;
            return;
        }
        priorEntry.setNextLink(header.getLink());
        header.setPrevLink(priorEntry.getLink());
        headerMap.put(header.getLink(), header);
        priorEntry = header;
    }

    @Override
    public HeaderData get(String key) {
        return headerMap.get(key);
    }
}
