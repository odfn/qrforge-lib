package com.nexus.qrs.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nexus.qrs.converter.bytestream.ByteStreamConverter;
import com.nexus.qrs.model.json.FileDataChainDeserializer;
import com.nexus.qrs.util.FileExtensions;
import org.dizitart.no2.collection.Document;
import org.dizitart.no2.collection.NitriteId;
import org.dizitart.no2.common.mapper.EntityConverter;
import org.dizitart.no2.common.mapper.NitriteMapper;
import org.dizitart.no2.index.IndexType;
import org.dizitart.no2.repository.annotations.Entity;
import org.dizitart.no2.repository.annotations.Id;
import org.dizitart.no2.repository.annotations.Index;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.nexus.qrs.IQRSContext.BlockCapacity;

/**
 * The FileDataChain stores arbitrary-length file data in blocks,
 * with each block containing a default maximum of 2148 bytes
 */
@Entity(indices = {
        @Index(fields = "chainName", type = IndexType.FULL_TEXT),
        @Index(fields = "chainType", type = IndexType.NON_UNIQUE),
        @Index(fields = "contentType", type = IndexType.NON_UNIQUE)
})
@JsonDeserialize(using = FileDataChainDeserializer.class)
public class FileDataChain {

    @Id
    private NitriteId id;

    private ArrayList<List<String>> dataChain;
    private final int linkCapacity;
    private final String chainName;
    private String contentType;
    private long fileSize;
    private long chainSize;
    private int padding;

    public FileDataChain(String chainName, BlockCapacity blockCapacity, int limiter) {
        this.dataChain = new ArrayList<>();
        this.linkCapacity = blockCapacity.value() - limiter;
        String extension = chainName.substring(chainName.lastIndexOf('.')+1);
        String type = FileExtensions.getInstance().queryExtension(extension);
        this.chainName = chainName;
        this.contentType = !extension.isEmpty() ?
                String.join("/", type, extension.toUpperCase()) : "STRING";
        this.padding = 0;
    }

    public FileDataChain(String chainName) {
        this(chainName, BlockCapacity.HEX, 2000);
    }

    public FileDataChain(String chainName, int limiter) {
        this(chainName, BlockCapacity.HEX, limiter);
    }

    public int getPadding() {
        return padding;
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }

    protected void setDataChain(ArrayList<List<String>> data) {
        dataChain = data;
    }

    public void addData(String input, ByteStreamConverter converter) {
        addData(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), converter);
    }

    public void addData(InputStream inStream, ByteStreamConverter converter) {
        try {
            int totalAvailable = inStream.available();

            int partitionCount = calculateNumPartitions(totalAvailable, linkCapacity);
            int partitionLength = Integer.toString(partitionCount).length();
            int sequenceCounterLength = (2 * partitionLength) + 3;
            int segmentCapacity = linkCapacity - sequenceCounterLength;
            partitionCount = calculateNumPartitions(totalAvailable, segmentCapacity);

            byte[] segment = new byte[segmentCapacity];
            String paddingFormat = String.format("%%%dd", partitionLength);
            String sequenceFormat = String.join("", "|" + paddingFormat + "/" + paddingFormat + "|");

            int counter = 0;
            do {
                inStream.read(segment, 0, Math.min(segment.length, inStream.available()));
                List<String> currentBlock = new ArrayList<>(processByteArray(segment, converter));
                String sequenceCounter = String.format(sequenceFormat, ++counter, partitionCount).replace(" ", "0");
                Collections.addAll(currentBlock, sequenceCounter.split(""));

                dataChain.add(currentBlock);
            } while (inStream.available() > 0);
            chainSize = dataChain.size();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static List<String> processByteArray(byte[] byteArray, ByteStreamConverter converter) {
        return IntStream.range(0, byteArray.length)
                .mapToObj(i -> converter.convert(byteArray[i] & 0xFF))
                .collect(Collectors.toList());
    }

    private int calculateNumPartitions(long fileSize, int partitionSize) {
        return (int) Math.ceil((double) fileSize / partitionSize);
    }

    /***
     * Adds data to the FileDataChain a link at a time
     * @param data a full link of data.
     */
    public void addLink(List<String> data) {
       List<String> newDataLink = new ArrayList<>(data);
       dataChain.add(newDataLink);
       chainSize = dataChain.size();
    }

    public NitriteId getId() {
        return id;
    }

    public void setId(NitriteId id) {
        this.id = id;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public List<List<String>> getContents() {
        return dataChain;
    }

    public int blockCount() {
        return dataChain.size();
    }

    public long fileSize() {
        return fileSize;
    }

    public long getChainSize() {
        return chainSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public void setChainSize(long chainSize) {
        this.chainSize = chainSize;
    }

    public String getChainName() {
        return chainName;
    }

    public String getContentType() {
        return contentType;
    }

    @Deprecated
    public Iterator<List<String>> chainIterator() {
        return dataChain.iterator();
    }

    @FunctionalInterface
    private interface SequenceFormatter {
//        Usage;
//        SequenceFormatter formatter = (c, pc) ->
//                    String.format("|%0" + partitionLength + "d/%0" + partitionLength + "d|", c, pc);
        String format(int counter, int partitionCount);
    }

    public static class Converter implements EntityConverter<FileDataChain> {

        @Override
        public Class<FileDataChain> getEntityType() {
            return FileDataChain.class;
        }

        @Override
        public Document toDocument(FileDataChain entity, NitriteMapper nitriteMapper) {
            return Document.createDocument()
                    .put("id", entity.getId())
                    .put("chainName", entity.getChainName())
                    .put("contentType", entity.getContentType())
                    .put("padding", entity.getPadding())
                    .put("dataChain", entity.getContents())
                    .put("fileSize", entity.fileSize())
                    .put("chainSize", entity.getChainSize());

        }

        @Override
        public FileDataChain fromDocument(Document document, NitriteMapper nitriteMapper) {

            FileDataChain dataChain = new FileDataChain(document.get("chainName", String.class));
            dataChain.setId(NitriteId.createId(document.get("id", String.class)));
            dataChain.setDataChain(document.get("dataChain", ArrayList.class));
            dataChain.setFileSize(document.get("fileSize", Long.class));
            dataChain.setChainSize(document.get("chainSize", Long.class));
            dataChain.setPadding(document.get("padding", Integer.class));
            dataChain.setContentType(document.get("contentType", String.class));
            return dataChain;
        }
    }
}
