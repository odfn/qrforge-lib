package com.nexus.qrs.model;

import com.nexus.qrs.IQRSContext;
import com.nexus.qrs.model.datamap.HeaderDataMap;
import com.nexus.qrs.util.FileExtensions;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;


/**
 * Deprecated in favor of the {@link HeaderDataMap}
 * */
@Deprecated
public class HeaderDataChain {

    private String dataLink;
    private final LinkedList<String> dataChain;
    private final int blockCapacity;
    private String chainName;
    private String contentType;
    private IQRSContext.ChainType chainType;
    private long fileSize;
    private long chainSize;

    private static final IQRSContext IQRS_CONTEXT = new IQRSContext() {};

    public HeaderDataChain(String chainName) {
        this(chainName, IQRSContext.BlockCapacity.HEX);
    }

    public HeaderDataChain(String chainName, IQRSContext.BlockCapacity blockCapacity) {
        this.dataLink = "";
        this.dataChain = new LinkedList<>(Collections.singletonList(dataLink));
        this.blockCapacity = blockCapacity.value();

        String extension = chainName.split("\\.").length > 1 ?
                chainName.split("\\.")[1] : "";
        String type = FileExtensions.getInstance().queryExtension(extension);
        this.chainName = chainName;
        this.contentType = !extension.isEmpty() ?
                String.join("/", type, extension.toUpperCase()) : "STRING";
        this.chainType = IQRSContext.ChainType.STANDARD;
    }
    public int blockCount() {
        return dataChain.size();
    }

    public long fileSize() {
        return fileSize;
    }

    public long chainSize() {
        return chainSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public void setChainSize(long chainSize) {
        this.chainSize = chainSize;
    }

    public String getChainName() {
        return chainName;
    }

    public String getContentType() {
        return contentType;
    }

    public IQRSContext.ChainType getChainType() {
        return chainType;
    }

    public Iterator<String> chainIterator() {
        return dataChain.iterator();
    }

    /***
     * Deprectated until refactored to store header info for each link in the FileDataChain
     * @param data
     */
    @Deprecated
    public void addData(String data) {
        //TODO: Refactor this implementation to store header info for each link in the FileDataChain
//        if (dataLink.size() < blockCapacity) {
//            dataLink.add(data);
//        } else {
//            dataLink = new ArrayList<>(Collections.singletonList(data));
//            dataChain.add(dataLink);
//        }
    }
}
