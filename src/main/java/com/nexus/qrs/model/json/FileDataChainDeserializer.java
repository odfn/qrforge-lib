package com.nexus.qrs.model.json;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.io.JsonEOFException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.nexus.qrs.model.FileDataChain;
import org.dizitart.no2.collection.NitriteId;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class FileDataChainDeserializer extends StdDeserializer<FileDataChain> {

    private ObjectMapper mapper;
    public FileDataChainDeserializer() {
        this(null);
        mapper = new ObjectMapper();
    }

    public FileDataChainDeserializer(Class<?> vc) {
        super(vc);
        mapper = new ObjectMapper();
    }

    @Override
    public FileDataChain deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        try {
            JsonNode result = mapper.readTree(p.getText());

            NitriteId chainId = (result.get("chainId").isNull() || result.get("chainId").isEmpty()) ?
                    NitriteId.newId() : NitriteId.createId(result.get("chainId").asText());
            String contentType = result.get("contentType").asText();
            String chainName = result.get("chainName").textValue();
            long fileSize = result.get("fileSize").asLong();
            JsonNode dataChain = result.get("dataChain");

            FileDataChain fileDataChain = new FileDataChain(chainName);
            fileDataChain.setId(chainId);
            fileDataChain.setContentType(contentType);
            fileDataChain.setFileSize(fileSize);

            for (Iterator<JsonNode> it = dataChain.elements(); it.hasNext(); ) {
                JsonNode chainLinkNode = it.next();
                List<String> chainLink = mapper.treeToValue(chainLinkNode,
                        TypeFactory.defaultInstance().constructCollectionType(List.class, String.class));
                fileDataChain.addLink(chainLink);
            }

            return fileDataChain;
        } catch (JsonEOFException e) {
            // Log the error and rethrow it or handle it accordingly
            System.err.println("JSON parsing error due to unexpected end of input: " + e.getMessage());
            System.err.println("Setting blank chain....");
            return new FileDataChain("blank_chain");
//            throw new IOException("Incomplete JSON data received", e);
        } catch (Exception e) {
            // Log the error and rethrow it or handle it accordingly
            System.err.println("Deserialization error: " + e.getMessage());
            throw new IOException("Error while deserializing FileDataChain", e);
        }
    }
}
