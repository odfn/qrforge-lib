package com.nexus.qrs.model.hash;

import com.google.zxing.common.BitMatrix;

public class SimpleBitMatrixHash implements QrHash {
    @Override
    public String getHash(BitMatrix bitMatrix) {
        return String.valueOf(bitMatrix.hashCode());
    }
}
