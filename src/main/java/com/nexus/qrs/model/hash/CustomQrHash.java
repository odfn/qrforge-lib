package com.nexus.qrs.model.hash;

import com.google.zxing.common.BitMatrix;

public class CustomQrHash implements QrHash {
    @Override
    public String getHash(BitMatrix bitMatrix) {
        return String.valueOf(Math.abs(bitMatrix.hashCode()));
    }
}
