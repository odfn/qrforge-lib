package com.nexus.qrs.model.hash;

import com.google.zxing.common.BitMatrix;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public interface QrHash {

    static SHA3.DigestSHA3 md = new SHA3.Digest224();
    String getHash(BitMatrix bitMatrix);

    //TODO: Consider using this as the hash implementation
    default String getHash(List<String> data, Long seed) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(String.join("", data).getBytes());
        bos.write(seed.toString().getBytes());
        md.update(bos.toByteArray());
        return Hex.toHexString(md.digest());
    }
}
