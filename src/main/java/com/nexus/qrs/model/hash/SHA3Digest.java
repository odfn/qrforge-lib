package com.nexus.qrs.model.hash;

import com.google.zxing.common.BitMatrix;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;

public class SHA3Digest implements QrHash {
    private SHA3.DigestSHA3 md;

    public SHA3Digest() {
        md = new SHA3.Digest224();
    }

    @Override
    public String getHash(BitMatrix bitMatrix) {
        md.update(bitMatrix.toString().getBytes());
        return Hex.toHexString(md.digest());
    }
}
